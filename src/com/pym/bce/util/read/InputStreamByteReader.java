package com.pym.bce.util.read;

import com.pym.bce.util.Uint32;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;

public class InputStreamByteReader implements ByteReader {

    private InputStream stream;
    public InputStreamByteReader(InputStream inputStream){
        this.stream = inputStream;
    }

    @Override
    public byte byteRead() throws IOException {
        byte[] b = new byte[1];
        stream.read(b);
        Byte result = new Byte(b[0]);
        return  result;
    }

    @Override
    public char charRead() throws IOException  {
        byte[] b = new byte[2];
        stream.read(b);
        // 高位左移8位，加上低位。
        Uint32 uint32 = new Uint32(b);
        Character character = new Character((char)uint32.toInt());
        return character;
    }

    @Override
    public short shortRead()  throws IOException {
        byte[] b = new byte[2];

        stream.read(b);
        // 高位左移8位，加上低位。
        Uint32 uint32 = new Uint32(b);
        return uint32.toShort();
    }


    @Override
    public int intRead() throws IOException  {
        byte[] b = new byte[4];
        stream.read(b);
        // 高位左移8位，加上低位。
        Uint32 uint32 = new Uint32(b);
        return uint32.toInt();
    }

    @Override
    public long longRead() throws IOException  {
        return 0;
    }

    @Override
    public float floatRead() throws IOException  {
        return 0;
    }

    @Override
    public double doubleRead() throws IOException  {
        return 0;
    }

    @Override
    public boolean booleanRead() throws IOException  {
        return true;
    }

    @Override
    public String stringRead(int length)  throws IOException {
        return null;
    }

    @Override
    public byte[] read(int size) throws IOException {
        byte[] bytes = new byte[size];
        stream.read(bytes);
        return bytes;
    }
}
