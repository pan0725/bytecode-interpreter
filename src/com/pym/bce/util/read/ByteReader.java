package com.pym.bce.util.read;

import java.io.IOException;

/**
 * 从数组中读取各种类型，为byte转其他类型提供便利。
 * */
public interface ByteReader {

    byte byteRead() throws IOException;
    char charRead()throws IOException;
    short shortRead()throws IOException;
    int intRead()throws IOException;
    long longRead()throws IOException;
    float floatRead()throws IOException;
    double doubleRead()throws IOException;
    boolean booleanRead()throws IOException;
    String stringRead(int length) throws IOException ;
    byte[] read(int size) throws IOException;
}
