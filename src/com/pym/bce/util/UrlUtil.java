package com.pym.bce.util;

import javax.swing.plaf.UIResource;

public class UrlUtil {

    public static String getFix(String url){
        if(StringUtil.isEmpty(url)){return "";}
        String[] temp = url.split("\\.");
        String fix = "";
        if(temp!=null && temp.length>1){
            fix = temp[temp.length-1];
        }
        return fix;
    }

    /**
     * 其他实现方式
     * */
    private  static String getFixBack(String url){
        if(StringUtil.isEmpty(url)){return "";}
        String fix = "";
        int index = url.lastIndexOf("\\.");
        if(index>0){
            fix = url.substring(index+1);
        }
        return fix;
    }
}
