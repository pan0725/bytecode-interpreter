package com.pym.bce.util;
 

/**
 * 将传入的有符号数转为无符号数，不过通常需要扩容才能做到。
 * */
public class Uint32 {

    byte[] data = new byte[4];

    /**
     *   构造Uint32.解析传入的参数，存放到byte数组中。
     * */
    public Uint32(byte b){
        data[3] = b;
    }
    public Uint32(short b){
        data[data.length-2] = (byte) (b >> 8);
        data[data.length-1] = (byte) (b & 0x00FF);
    }
    public Uint32(int b){
        data[data.length-4] = (byte) (b >> 24);
        data[data.length-3] = (byte) (b >> 16);
        data[data.length-2] = (byte) (b >> 8);
        data[data.length-1] = (byte) (b & 0x000000FF);
    }
    public Uint32(byte[] bytes){
        // 大于4位: Uint与int相同，要将前面的过滤掉，就像int转为byte一样，低位置保留，高位置舍弃
        // 等于4位
        // 小于4位
       int targetTop = 0;
       if(bytes !=null && bytes.length>0){
           targetTop = bytes.length-1;
           for(int top=3; top>=0 && targetTop>=0 ; top--,targetTop--){
               data[top] = bytes[targetTop];
           }
       }
    }
    public Uint32(byte[] bytes, int start){
        int targetTop = 0;
        int targetLow = 0;
        if(bytes !=null && bytes.length>0){
            targetTop = bytes.length-1;
            targetLow = start;
            for(int top=3; top>=0 && targetTop>=targetLow ;top--,targetTop--){
                data[top] = bytes[targetTop];
            }
        }
    }
    public Uint32(byte[] bytes, int start, int length){
        int targetTop = start+length-1;
        int targetLow = start;
        if(bytes !=null && bytes.length>0){
            for(int top=3; top>=0 && targetTop>=targetLow ;top--,targetTop--){
                data[top] = bytes[targetTop];
            }
        }
    }

    public byte toByte(){
       return data[data.length-1];
    }

    public short toShort(){
        byte[] b = data;
        // 高位左移8位，加上低位。
        int result =  (byteToUint(b[b.length-2])<<8)
                     +(byteToUint(b[b.length-1]));
        return (short) result;
    }

    public int toInt(){
        byte[] b = data;
//        stream.read(b);
        // 高位左移8位，加上低位。
        int result =  (byteToUint(b[0])<<24)
                +(byteToUint(b[1])<<16)
                +(byteToUint(b[2])<<8)
                +(byteToUint(b[3]));
        return result;
    }

    public long toLong(){
        return toInt();
    }

    public float toFloat(){
        return 0;
    }

    public double toDouble(){
        return 0;
    }

    private static int byteToUint(byte b){
        int i = 0x000000FF;
        int s = i & b;
        return s;
    }
    /**
     * 进行计算后，byte自动转为了int，所以这里传入的int其实只有低8位是有效的。
     * */
    private static int byteToUint(int b){
        int i = 0x000000FF;
        int s = i & b;
        return s;
    }
}
