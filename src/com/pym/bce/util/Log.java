package com.pym.bce.util;

public class Log {
    static volatile Log log = new Log();
    private Log(){}
    public static Log log(){
        return  log;
    }

    public void info(String str){
        System.out.println(str);
    }

    public void error(String str){
        System.out.println(str);
    }

    public void warn(String str){
        System.out.println(str);
    }

    public void debug(String str){
        System.out.println(str);
    }

}




