package com.pym.bce.util;

public class StringUtil {

    public static boolean isEmpty(String str){
        if(str == null || "".equals(str)){
            return true;
        }
        return false;
    }
    public static boolean isBlank(String str){
        if(isEmpty(str) && !str.trim().equals("")){
            return true;
        }
        return false;
    }
}
