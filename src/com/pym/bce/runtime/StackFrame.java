package com.pym.bce.runtime;


import com.pym.bce.classfile.item.constant.ConstantPoolItem;
import com.pym.bce.mate.ConstantPoolMate;
import com.pym.bce.mate.KlassMate;
import com.pym.bce.mate.MethodMate;

public class StackFrame {



    public SimpleStack variateTable;
    public SimpleStack operandStack;
    public MethodMate methodConst;
    public KlassMate klass;
    public ConstantPoolMate constantPool;

    public StackFrame next;

    StackFrame(){}
    public StackFrame(MethodMate method){
        variateTable = new SimpleStack(method.getMaxLocals());
        operandStack = new SimpleStack(method.getMaxLocals());
        methodConst = method;
        klass = methodConst.getKlass();
        constantPool = klass.getConstantPoolMate();
    }
}
