package com.pym.bce.runtime;

import java.util.Stack;

public class SimpleStack  {

    Object[] objects ;
    int index = 0;
    public SimpleStack(int size){
        objects = new Object[size];
    }
    public void push(Object obj){
        objects[index++] = obj;
        if(index>=objects.length){
            index = objects.length;
        }
    }
    public Object pop(){
        index--;
        return objects[index];
    }
    public void add(int idx,Object obj){
        if(idx<objects.length){
            objects[idx]=obj;
        }
    }
    public Object get(int idx){
        if(idx<objects.length){
            return objects[idx];
        }
        return null;
    }

    public Object peek() {
        return objects[index-1];
    }
}
