package com.pym.bce.runtime;

import com.pym.bce.util.Log;

public class ThreadStack {

    private StackFrame top;

    public StackFrame popStackFrame(){
        StackFrame newTop = top.next;
        top.next = null;
        top = newTop;
        return top;
    }
    public void pushStackFrame(StackFrame newTop){
        if(newTop!=null){
            newTop.next = top;
            top = newTop;
        }else {
            Log.log().error("ThreadStack.pushStackFrame方法不接受空值");
        }
    }
    public StackFrame top(){
        return  top;
    }
}
