package com.pym.bce.interpreter;
import com.pym.bce.util.Uint32;

/**
 * @author panyuming
 */
public class DefaultBytecodeManager implements BytecodeManager{

    byte[] codes;
    int point = 0;

    public DefaultBytecodeManager(byte[] codes){
        this.codes = codes;
    }
    @Override
    public Code nextBytecode() {
        Code code = null;
        if(codes!=null && point<codes.length){
            int bytecode = new Uint32(codes[point++]).toInt();
            code = Code.values()[bytecode];
        }
        return code;
    }

    /**
     * 以下方法不需要判断指针越界，因为一定是先访问nextBytecode，获取字节码，然后才能根据字节码读取不同字节。
     * */

    @Override
    public byte readByte() {
        return codes[point++];
    }

    @Override
    public short read2Byte() {
        Uint32 uint = new Uint32(codes,point,2);
        point+=2;
        return uint.toShort();
    }

    @Override
    public int read4Byte() {
        Uint32 uint = new Uint32(codes,point,4);
        point+=4;
        return uint.toInt();
    }
}
