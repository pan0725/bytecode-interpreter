package com.pym.bce.interpreter;

import com.pym.bce.interpreter.Bytecode;

/**
 * @author panyuming
 */
public interface BytecodeManager {
    /**
     * 返回下一条字节码指令
     * @return 字节码指令的封装
     * */
    Code nextBytecode();

    byte readByte();

    short read2Byte();

    int read4Byte();

}
