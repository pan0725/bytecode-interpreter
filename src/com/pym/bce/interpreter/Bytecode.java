package com.pym.bce.interpreter;

import com.pym.bce.interpreter.Code;

/**
 * @author panyuming
 */
public abstract class Bytecode {

    private Code code;
    public Code getCode(){
        return code;
    }
    /**
     * 不同字节码的执行逻辑
     * */
    public abstract void execute();

}
