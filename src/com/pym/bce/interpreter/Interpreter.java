package com.pym.bce.interpreter;

/**
 * @author panyuming
 */
public interface Interpreter {
    /**
     * 执行字节码
     * @param manager 指令的提供者
     * */
    void loop(BytecodeManager manager) throws Exception;
}
