package com.pym.bce.interpreter;

/**
 * @author panyuming
 */
public enum Code {


    nop             (0),
    aconst_null     (1),
    iconst_m1       (2),
    iconst_0        (3),
    iconst_1        (4),
    iconst_2        (5),
    iconst_3        (6),
    iconst_4        (7),
    iconst_5        (8),
    lconst_0        (9),
    lconst_1        (10),
    fconst_0        (11),
    fconst_1        (12),
    fconst_2        (13),
    dconst_0        (14),
    dconst_1        (15),
    bipush          (16),
    sipush          (17),
    ldc             (18),
    ldc_w           (19),
    ldc2_w          (20),
    iload           (21),
    lload            (22),
    fload           (23),
    dload           (24),
    aload           (25),
    iload_0         (26),
    iload_1         (27),
    iload_2         (28),
    iload_3         (29),
    lload_0         (30),
    lload_1         (31),
    lload_2         (32),
    lload_3         (33),
    fload_0         (34),
    fload_1         (35),
    fload_2         (36),
    fload_3         (37),
    dload_0         (38),
    dload_1         (39),
    dload_2         (40),
    dload_3         (41),
    aload_0         (42),
    aload_1         (43),
    aload_2         (44),
    aload_3         (45),
    iaload          (46) ,
    laload          (47) ,
    faload           (48),
    daload           (49),
     aaload          (50),
    baload           (51),
    caload           (52),
    saload           (53),
    istore           (54),
    lstore           (55),
    fstore           (56),
    dstore           (57),
    astore           (58),
    istore_0         (59),
    istore_1         (60),
    istore_2         (61),
    istore_3         (62),
    lstore_0         (63),
    lstore_1         (64),
    lstore_2         (65),
    lstore_3         (66),
    fstore_0         (67),
    fstore_1         (68),
    fstore_2         (69),
    fstore_3         (70),
    dstore_0         (71),
    dstore_1         (72),
    dstore_2         (73),
    dstore_3         (74),
    astore_0         (75),
    astore_1         (76),
    astore_2         (77),
    astore_3         (78),
    iastore          (79),
    lastore          (80),
    fastore          (81),
    dastore          (82),
    aastore          (83),
    bastore          (84),
    castore          (85),
    sastore          (86),
    pop              (87),
    pop2             (88),
    dup              (89),
    dup_xl           (90),
    dup_x2           (91),
    dup2             (92),
    dup2_x1          (93),
    dup2_x2          (94),
    swap             (95),
    iadd             (96),
    ladd             (97),
    fadd             (98),
    dadd             (99),
    isub             (100),
    lsub             (101),
    fsub             (102),
    dsub             (103),   //将栈顶两double类型数值相减并将结果压入栈顶
    imul             (104),   //将栈顶两int类型数值相乘并将结果压入栈顶
    lmul             (105),   //将栈顶两long类型数值相乘并将结果压入栈顶
    fmul             (106),   //将栈顶两float类型数值相乘并将结果压入栈顶
    dmul             (107),   //将栈顶两double类型数值相乘并将结果压入栈顶
    idiv             (108),   //将栈顶两int类型数值相除并将结果压入栈顶
    ldiv             (109),   //将栈顶两long类型数值相除并将结果压入栈顶
    fdiv             (110),   //将栈顶两float类型数值相除并将结果压入栈顶
    ddiv             (111),   //将栈顶两double类型数值相除并将结果压入栈顶
    irem             (112),   //将栈顶两int类型数值作取模运算并将结果压入栈顶
    lrem             (113),   //将栈顶两long类型数值作取模运算并将结果压入栈顶
    frem             (114),   //将栈顶两float类型数值作取模运算并将结果压入栈顶
    drem             (115),   //将栈顶两double类型数值作取模运算并将结果压入栈顶
    ineg             (116),   //将栈顶int类型数值取负并将结果压入栈顶
    lneg             (117),   //将栈顶long类型数值取负并将结果压入栈顶
    fneg             (118),	  //将栈float类型数值取负并将结果压入栈顶
    dneg             (119),   //将栈顶double类型数值取负并将结果压入栈顶
    ishl             (0x78),  //将int型数值左移指定位数并将结果压入栈顶
    lshl             (0x79),  //将long型数值左移指定位数并将结果压入栈顶
    ishr             (0x7a),  //将int型数值右(带符号)移指定位数并将结果压入栈顶
    lshr             (0x7b),  //将long型数值右(带符号)移指定位数并将结果压入栈顶
    iushr            (0x7c),  //将int型数值右(无符号)移指定位数并将结果压入栈顶
    lushr            (0x7d),  //将long型数值右(无符号)移指定位数并将结果压入栈顶
    iand             (0x7e),  //将栈顶两int型数值"按位与"并将结果压入栈顶
    land             (0x7f),  //将栈顶两long型数值"按位与"并将结果压入栈顶
    ior	             (0x80),  //将栈顶两int型数值"按位或"并将结果压入栈顶
    lor	             (0x81),  //将栈顶两long型数值"按位或"并将结果压入栈顶
    ixor             (0x82),  //将栈顶两int型数值"按位异或"并将结果压入栈顶    ;
    lxor             (0x83),  //将栈顶两long型数值"按位异或"并将结果压入栈顶
    iinc             (0x84),  //将指定int型变量增加指定值(如i++, i--, i+=2等)
    i2l	             (0x85),  //将栈顶int型数值强制转换为long型数值并将结果压入栈顶
    i2f	             (0x86),  //将栈顶int型数值强制转换为float型数值并将结果压入栈顶
    i2d	             (0x87),  //将栈顶int型数值强制转换为double型数值并将结果压入栈顶
    l2i	             (0x88),  //将栈顶long型数值强制转换为int型数值并将结果压入栈顶
    l2f	             (0x89),  //将栈顶long型数值强制转换为float型数值并将结果压入栈顶
    l2d	             (0x8a),  //将栈顶long型数值强制转换为double型数值并将结果压入栈顶
    f2i	             (0x8b),  //将栈顶float型数值强制转换为int型数值并将结果压入栈顶
    f2l	             (0x8c),  //将栈顶float型数值强制转换为long型数值并将结果压入栈顶}
    f2d	             (0x8d),  //将栈顶float型数值强制转换为double型数值并将结果压入栈顶
    d2i	             (0x8e),  //将栈顶double型数值强制转换为int型数值并将结果压入栈顶
    d2l	             (0x8f),  //将栈顶double型数值强制转换为long型数值并将结果压入栈顶
    d2f	             (0x90),  //将栈顶double型数值强制转换为float型数值并将结果压入栈顶
    i2b	             (0x91),  //将栈顶int型数值强制转换为byte型数值并将结果压入栈顶
    i2c	             (0x92),  //将栈顶int型数值强制转换为char型数值并将结果压入栈顶
    i2s	             (0x93),  //将栈顶int型数值强制转换为short型数值并将结果压入栈顶
    lcmp        	 (0x94),  //比较栈顶两long型数值大小, 并将结果(1, 0或-1)压入栈顶
    fcmpl       	 (0x95),  //比较栈顶两float型数值大小, 并将结果(1, 0或-1)压入栈顶; 当其中一个数值为NaN时, 将-1压入栈顶
    fcmpg       	 (0x96),  //比较栈顶两float型数值大小, 并将结果(1, 0或-1)压入栈顶; 当其中一个数值为NaN时, 将1压入栈顶
    dcmpl       	 (0x97),  //比较栈顶两double型数值大小, 并将结果(1, 0或-1)压入栈顶; 当其中一个数值为NaN时, 将-1压入栈顶
    dcmpg       	 (0x98),  //比较栈顶两double型数值大小, 并将结果(1, 0或-1)压入栈顶; 当其中一个数值为NaN时, 将1压入栈顶
    ifeq        	 (0x99),  //当栈顶int型数值等于0时跳转
    ifne        	 (0x9a),  //当栈顶int型数值不等于0时跳转
    iflt        	 (0x9b),  //当栈顶int型数值小于0时跳转
    ifge        	 (0x9c),  //当栈顶int型数值大于等于0时跳转
    ifgt        	 (0x9d),  //当栈顶int型数值大于0时跳转
    ifle        	 (0x9e),  //当栈顶int型数值小于等于0时跳转
    if_icmpeq   	 (0x9f),  //比较栈顶两int型数值大小, 当结果等于0时跳转
    if_icmpne   	 (0xa0),  //比较栈顶两int型数值大小, 当结果不等于0时跳转
    if_icmplt   	 (0xa1),  //比较栈顶两int型数值大小, 当结果小于0时跳转
    if_icmpge   	 (0xa2),  //比较栈顶两int型数值大小, 当结果大于等于0时跳转
    if_icmpgt   	 (0xa3),  //比较栈顶两int型数值大小, 当结果大于0时跳转
    if_icmple   	 (0xa4),  //比较栈顶两int型数值大小, 当结果小于等于0时跳转
    if_acmpeq   	 (0xa5),  //比较栈顶两引用型数值, 当结果相等时跳转
    if_acmpne   	 (0xa6),  //比较栈顶两引用型数值, 当结果不相等时跳转
	goto_        	 (0xa7),  //无条件跳转
    jsr	             (0xa8),  //跳转至指定的16位offset位置, 并将jsr的下一条指令地址压入栈顶
    ret	             (0xa9),  //返回至本地变量指定的index的指令位置(一般与jsr或jsr_w联合使用)
    tableswitch	     (0xaa),  //用于switch条件跳转, case值连续(可变长度指令)
    lookupswitch	 (0xab),  //用于switch条件跳转, case值不连续(可变长度指令)
    ireturn	         (0xac),  //从当前方法返回int
    lreturn	         (0xad),  //从当前方法返回long
    freturn	         (0xae),  //从当前方法返回float
    dreturn	         (0xaf),  //从当前方法返回double
    areturn	         (0xb0),  //从当前方法返回对象引用
	return_	         (0xb1),  //从当前方法返回void
    getstatic   	 (0xb2),  //获取指定类的静态域, 并将其压入栈顶
    putstatic   	 (0xb3),  //为指定类的静态域赋值
    getfield    	 (0xb4),  //获取指定类的实例域, 并将其压入栈顶
    putfield    	 (0xb5),  //为指定类的实例域赋值
    invokevirtual	 (0xb6),  //调用实例方法
    invokespecial	 (0xb7),  //调用超类构建方法, 实例初始化方法, 私有方法
    invokestatic	 (0xb8),  //调用静态方法
    invokeinterface	 (0xb9),  //调用接口方法
    invokedynamic	 (0xba),  //调用动态方法
	new_	         (0xbb),  //创建一个对象, 并将其引用引用值压入栈顶
    newarray    	 (0xbc),  //创建一个指定的原始类型(如int, float, char等)的数组, 并将其引用值压入栈顶
    anewarray   	 (0xbd),  //创建一个引用型(如类, 接口, 数组)的数组, 并将其引用值压入栈顶
    arraylength      (0xbe),  //获取数组的长度值并压入栈顶
    athrow	         (0xbf),  //将栈顶的异常抛出
    checkcast   	 (0xc0),  //检验类型转换, 检验未通过将抛出 ClassCastException
	instanceof_	     (0xc1),  //检验对象是否是指定类的实际, 如果是将1压入栈顶, 否则将0压入栈顶
    monitorenter	 (0xc2),  //获得对象的锁, 用于同步方法或同步块
    monitorexit	     (0xc3),  //释放对象的锁, 用于同步方法或同步块
    wide        	 (0xc4),  //扩展本地变量的宽度
    multianewarray	 (0xc5),  //创建指定类型和指定维度的多维数组(执行该指令时, 操作栈中必须包含各维度的长度值), 并将其引用压入栈顶
    ifnull	         (0xc6),  //为null时跳转
    ifnonnull	     (0xc7),  //不为null时跳转
    goto_w	         (0xc8),  //无条件跳转(宽索引)

    ;

    int code;

    Code(int code){
        this.code = code;
    }
}

