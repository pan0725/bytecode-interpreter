package com.pym.bce.mate;

import com.pym.bce.classfile.item.attribute.AttributeItem;

public class FieldMate {

    private AttributeItem[] attributeItems;
    private KlassMate klass;
    private short accessFlags;
    private String name;
    private String descriptor;

    Object value;

    public AttributeItem[] getAttributeItems() {
        return attributeItems;
    }

    public void setAttributeItems(AttributeItem[] attributeItems) {
        this.attributeItems = attributeItems;
    }

    public KlassMate getKlass() {
        return klass;
    }

    public void setKlass(KlassMate klass) {
        this.klass = klass;
    }

    public short getAccessFlags() {
        return accessFlags;
    }

    public void setAccessFlags(short accessFlags) {
        this.accessFlags = accessFlags;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescriptor() {
        return descriptor;
    }

    public void setDescriptor(String descriptor) {
        this.descriptor = descriptor;
    }

    public void set(Object o) {
        // TODO 类型校验
        value = o;
    }
    public Object get() {
        // TODO 类型校验
        return value;
    }


}
