package com.pym.bce.mate;

import com.pym.bce.classfile.item.constant.ConstantPoolItem;

import java.io.IOException;

public class ConstantPoolMate {

    ConstantPoolItem[] constantPoolItems ;

    public ConstantPoolMate(ConstantPoolItem[] items){
        this.constantPoolItems = items;
    }

    public Object get(short index) throws IOException {
        ConstantPoolItem item = constantPoolItems[index];
        return item.getValue(constantPoolItems);
    }


}
