package com.pym.bce.mate;

public class InstanceData {

    private KlassMate klassMate;
    private FieldMate[] fieldMate;
    private MethodMate[] methodMates;

    public KlassMate getKlassMate() {
        return klassMate;
    }

    public void setKlassMate(KlassMate klassMate) {
        this.klassMate = klassMate;
    }

    public FieldMate[] getFieldMate() {
        return fieldMate;
    }

    public void setFieldMate(FieldMate[] fieldMate) {
        this.fieldMate = fieldMate;
    }

    public MethodMate[] getMethodMates() {
        return methodMates;
    }

    public void setMethodMates(MethodMate[] methodMates) {
        this.methodMates = methodMates;
    }

    public FieldMate getFieldByName(String name) {
        if(fieldMate!=null){
            for (FieldMate mate : fieldMate) {
                if(mate.getName().equals(name)){
                    return mate;
                }
            }
        }
        return null;
    }
}
