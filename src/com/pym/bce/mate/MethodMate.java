package com.pym.bce.mate;

import com.pym.bce.classfile.item.attribute.AttributeItem;

/**
 * TODO 取一个更好的名称
 * */
public class MethodMate implements Mate {

    private byte[] code;
    private short maxStack;
    private short maxLocals;
//    private short exceptionTableLength;
//    private CodeAttributeItem.ExceptionTable[] exceptionTable;
    private AttributeItem[] attributeItems;
    private KlassMate klass;
    private Object[] variables;

    private short accessFlags;
    private String name;
    private String descriptor;

    public byte[] getCode() {
        return code;
    }

    public void setCode(byte[] code) {
        this.code = code;
    }

    public short getMaxStack() {
        return maxStack;
    }

    public void setMaxStack(short maxStack) {
        this.maxStack = maxStack;
    }

    public short getMaxLocals() {
        return maxLocals;
    }

    public void setMaxLocals(short maxLocals) {
        this.maxLocals = maxLocals;
    }

    public AttributeItem[] getAttributeItems() {
        return attributeItems;
    }

    public void setAttributeItems(AttributeItem[] attributeItems) {
        this.attributeItems = attributeItems;
    }

    public KlassMate getKlass() {
        return klass;
    }

    public void setKlass(KlassMate klass) {
        this.klass = klass;
    }

    public Object[] getVariables() {
        return variables;
    }

    public void setVariables(Object[] variables) {
        this.variables = variables;
    }

    public short getAccessFlags() {
        return accessFlags;
    }

    public void setAccessFlags(short accessFlags) {
        this.accessFlags = accessFlags;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescriptor() {
        return descriptor;
    }

    public void setDescriptor(String descriptor) {
        this.descriptor = descriptor;
    }
}
