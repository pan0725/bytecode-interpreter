package com.pym.bce.mate;


import java.util.ArrayList;
import java.util.List;

public class KlassMate implements Mate{

    private ConstantPoolMate constantPool;
    private KlassMate superKlass;
    private KlassMate[] interfaces;
    private FieldMate[] fields;
    private MethodMate[] methods;
    private String name;
    // 属性
    private MethodMate[] nonStaticMethod;
    private FieldMate[] nonStaticField;


    public ConstantPoolMate getConstantPoolMate() {
        return constantPool;
    }

    public void setConstantPoolMate(ConstantPoolMate constantPoolMate) {
        this.constantPool = constantPoolMate;
    }

    public KlassMate getSuperKlass() {
        return superKlass;
    }

    public void setSuperKlass(KlassMate superKlass) {
        this.superKlass = superKlass;
    }

    public KlassMate[] getInterfaces() {
        return interfaces;
    }

    public void setInterfaces(KlassMate[] interfaces) {
        this.interfaces = interfaces;
    }

    public FieldMate[] getFields() {
        return fields;
    }

    public void setFields(FieldMate[] fields) {
        this.fields = fields;
        List<FieldMate> list = new ArrayList();
        if(this.fields!=null){
            for(FieldMate mate :fields){
                if((mate.getAccessFlags() & 0x0008)==0){
                    list.add(mate);
                }
            }
        }
        this.nonStaticField = new FieldMate[list.size()];
        for(int i=0;i<list.size();i++){
            nonStaticField[i] = list.get(i);
        }
    }

    public MethodMate[] getMethods() {
        return methods;
    }

    public void setMethods(MethodMate[] methods) {
        this.methods = methods;
    }

    public ConstantPoolMate getConstantPool() {
        return constantPool;
    }

    public void setConstantPool(ConstantPoolMate constantPool) {
        this.constantPool = constantPool;
    }

    public MethodMate[] getNonStaticMethod() {
        return nonStaticMethod;
    }

    public void setNonStaticMethod(MethodMate[] nonStaticMethod) {
        this.nonStaticMethod = nonStaticMethod;
    }

    public FieldMate[] getNonStaticField() {
        return nonStaticField;
    }

    public void setNonStaticField(FieldMate[] nonStaticField) {
        this.nonStaticField = nonStaticField;
    }

    public void setName(String name) {
        this.name = name;
    }

    public InstanceData allocate(){
        InstanceData data = new InstanceData();
        data.setFieldMate(getNonStaticField());
        data.setMethodMates(getNonStaticMethod());
        data.setKlassMate(this);
        return data;
    }
}
