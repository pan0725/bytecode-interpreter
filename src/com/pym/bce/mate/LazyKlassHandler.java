package com.pym.bce.mate;

import com.pym.bce.classfile.ClassLoader;
import com.pym.bce.util.Log;

import java.io.IOException;

public class LazyKlassHandler {

    private KlassMate val;
    private String className;

    public LazyKlassHandler(String className){
        this.className = className;
    }

    public KlassMate val() throws IOException {
        if(val == null){
            KlassMate klassMate = ClassLoader.classLoader().loadClass(className);
            if(klassMate!=null){
                this.val = klassMate;
            }else{
                Log.log().error(className+" not found");
            }
        }
        return val;
    }
}
