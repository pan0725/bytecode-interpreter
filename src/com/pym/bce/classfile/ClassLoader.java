package com.pym.bce.classfile;

import com.pym.bce.classfile.classpath.ClassNode;
import com.pym.bce.classfile.classpath.ClassNodeFactory;
import com.pym.bce.classfile.classpath.ClassPath;
import com.pym.bce.mate.KlassMate;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClassLoader {

    private ClassLoader(){};
    private static volatile ClassLoader classLoader = new ClassLoader();
    private Map<String,KlassMate> cache = new HashMap();

    List<ClassNode> classPaths = new ArrayList();

    public KlassMate loadClass(String className) throws IOException {
        KlassMate mate = cache.get(className);
        if(mate!=null){
            return mate;
        }
        InputStream inputStream = null;
        for(ClassNode classPath :classPaths){
            inputStream = classPath.readClassFile(className);
            if(inputStream!=null){
                break;
            }
        }
        mate = ParseKlass.parseKlass(inputStream);
        cache.put(className,mate);
        return mate;
    }

    public void addClassPath(String classPathUrl){
        ClassNode classPath = ClassNodeFactory.getClassPath(new File(classPathUrl));
        classPaths.add(classPath);
    }

    public static ClassLoader classLoader(){
        return classLoader;
    }
}
