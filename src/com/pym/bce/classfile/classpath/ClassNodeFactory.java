package com.pym.bce.classfile.classpath;

import com.pym.bce.util.StringUtil;
import com.pym.bce.util.UrlUtil;

import java.io.File;

public class ClassNodeFactory {

    protected static ClassNode getClassNode(String fileUrl){
        String fix = UrlUtil.getFix(fileUrl);
        ClassNode classNode = null;
        if( StringUtil.isEmpty(fix) ){
            // 没有后缀名称，普通文件夹,扫描所有的文件
            classNode = new FolderClassNode(new File(fileUrl));
        }else{
            switch (fix) {
                case "jar":
                case "zip":
                case "class":// 单独一个文件
            }
        }
        return classNode;
    }



    protected static ClassNode getClassNode(File file){
        String fix = UrlUtil.getFix(file.getName());
        ClassNode classNode = null;
        if( StringUtil.isEmpty(fix) ){
            // 没有后缀名称，普通文件夹,扫描所有的文件
            classNode = new FolderClassNode( file );
        }else{
            switch (fix) {
                case "jar":
                    break;
                case "zip":
                    break;
                case "class":// 单独一个文件
                    classNode = new DirectClassNode(file);
                    break;
                default: ;
            }
        }
        return classNode;
    }

    public static ClassNode getClassPath(File file){
        String fix = UrlUtil.getFix(file.getName());
        ClassNode classNode = null;
        if( StringUtil.isEmpty(fix) ){
            // 没有后缀名称，普通文件夹,扫描所有的文件
            classNode = new ClassPath( file );
        }else{
            switch (fix) {
                case "jar":
                    break;
                case "zip":
                    break;
                case "class":// 单独一个文件
                    classNode = new DirectClassNode(file);
                    break;
                default: ;
            }
        }
        return classNode;
    }
//    //
//    abstract ClassNode scanClassNode(String url);
}
