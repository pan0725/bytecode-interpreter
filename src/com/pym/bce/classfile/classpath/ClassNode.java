package com.pym.bce.classfile.classpath;

import com.pym.bce.classfile.ClassFile;

import java.io.InputStream;

public abstract class ClassNode {

    String path;

    /**
     * 再classpath中找寻class文件
     * @param className class文件的名称，传入的必须是全类路径名，即包名加类名。
     * @return class文件流
     * */
    public abstract InputStream readClassFile(String className);

}
