package com.pym.bce.classfile.classpath;

import com.pym.bce.util.StringUtil;
import com.pym.bce.util.UrlUtil;
import com.sun.security.sasl.ntlm.FactoryImpl;
import com.sun.xml.internal.ws.util.StringUtils;

import java.io.*;
import java.lang.reflect.Field;

/**
 *  本模块组合模式中最小单位，代表了一个class文件
 * */
public class DirectClassNode extends ClassNode{

    File file;

    public DirectClassNode(File file){
        this.file = file;
        this.path = file.getName();
        // TODO 定义访问协议，全局遵守
        this.path = path.replace(".class","");
    }

    @Override
    public InputStream readClassFile(String className) {
        // 确定是否是.class文件  TODO 文件搜索是否需要校验？
//        String fix = UrlUtil.getFix(className);
//        if(StringUtil.isEmpty(fix) || !"class".equals(fix) ){
//            return null;
//        }
        try {
            if(this.path.equals(className)){
                FileInputStream stream = new FileInputStream(file);
                BufferedInputStream buffer = new BufferedInputStream(stream);
                return buffer;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
