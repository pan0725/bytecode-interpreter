package com.pym.bce.classfile.classpath;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * ClassPath是组合模式的顶层设计，这样做是基于一个前提，ClassPath与普通的FolderClassNode对象有些不同，
 * 它有更多的特殊的操作，例如ClassPath的path属性应该是 "/" ， 所以不能和FolderClassNode混为一谈。
 * */
public class ClassPath extends ClassNode {


    List<ClassNode> childNode = new ArrayList<>();

    String fileUri;
    private ClassPath(){}
    public ClassPath(String file){
        this(new File(file));

    }
    public ClassPath(File file){
        this.fileUri = file.getPath();
        this.path = "/";
        // 扫描文件夹
        File classpath = new File(fileUri);
        if(classpath != null){
            File[] files = classpath.listFiles();
            if(files!=null && files.length>0){
                for(File temp :files){
                    ClassNode classNode = ClassNodeFactory.getClassNode(temp);
                    if(classNode==null){
                        continue;
                    }
                    childNode.add(classNode);
                }
            }
        }
    }


    @Override
    public InputStream readClassFile(String className) {
        // classpath就是首地址，
        InputStream inputStream = null;
        if(childNode!=null){
            Iterator<ClassNode> iterator =  childNode.iterator();
            while(iterator.hasNext() && inputStream==null){
                inputStream = iterator.next().readClassFile(className.replaceFirst(path+".",""));
            }
        }
        return inputStream;
    }
}
