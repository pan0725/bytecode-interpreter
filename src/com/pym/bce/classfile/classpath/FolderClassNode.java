package com.pym.bce.classfile.classpath;

import com.sun.istack.internal.NotNull;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class FolderClassNode extends ClassNode{

    List<ClassNode> childNode = new ArrayList<>();

    String fileUri;
    public FolderClassNode(File file){
        this.fileUri = file.getPath();
        this.path = file.getName();
        // 扫描文件夹
        File classpath = new File(fileUri);
        if(classpath != null){
            File[] files = classpath.listFiles();
            if(files!=null && files.length>0){
                for(File temp :files){
                    ClassNode classNode = ClassNodeFactory.getClassNode(temp);
                    if(classNode==null){
                        continue;
                    }
                    childNode.add(classNode);
                }
            }
        }
    }


    @Override
    public InputStream readClassFile(String className) {
        boolean forMe = className.startsWith(path);
        InputStream inputStream = null;
        if(childNode!=null && forMe){
            Iterator<ClassNode> iterator =  childNode.iterator();
            while(iterator.hasNext() && inputStream==null){
                inputStream = iterator.next().readClassFile(className.replaceFirst(path+".",""));
            }
        }
        return inputStream;
    }
}
