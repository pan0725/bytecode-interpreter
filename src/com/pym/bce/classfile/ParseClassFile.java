package com.pym.bce.classfile;

import com.pym.bce.classfile.item.FieldItem;
import com.pym.bce.classfile.item.MethodItem;
import com.pym.bce.classfile.item.attribute.AttributeItem;
import com.pym.bce.classfile.item.attribute.AttributeItemFacotry;
import com.pym.bce.classfile.item.attribute.CodeAttributeItem;
import com.pym.bce.classfile.item.constant.ConstantClassInfo;
import com.pym.bce.classfile.item.constant.ConstantItemFactory;
import com.pym.bce.classfile.item.constant.ConstantPoolItem;
import com.pym.bce.mate.FieldMate;
import com.pym.bce.mate.KlassMate;
import com.pym.bce.mate.MethodMate;
import com.pym.bce.util.Log;
import com.pym.bce.util.read.ByteReader;
import com.pym.bce.util.read.InputStreamByteReader;

import java.io.IOException;
import java.io.InputStream;

public class ParseClassFile {

    static Log log = Log.log();

    /**
     *  将class二进制解析到内存中。
     * */
    public static ClassFile parse(InputStream stream) throws IOException {
        if(stream == null){
            // 文件未找到,应抛出异常
            return null;
        }
        ByteReader reader = new InputStreamByteReader(stream);
        ClassFile classFile = doParse(reader);

        return classFile;
    }

    private static ClassFile doParse(ByteReader reader) throws IOException {
        ClassFile classFile = new ClassFile();
        parseMagic(reader,classFile);
        parseMinorVersion(reader,classFile);
        parseMajroVersion(reader,classFile);
        parseConstantPoolCount(reader,classFile);
        parseConstantPool(reader,classFile);
        parseAccessFlags(reader,classFile);
        parseThisClass(reader,classFile);
        parseSuperClass(reader,classFile);
        parseInterfacesCount(reader,classFile);
        parseInterfaces(reader,classFile);
        parseFieldsCount(reader,classFile);
        parseFields(reader,classFile);
        parseMethodCount(reader,classFile);
        parseMethods(reader,classFile);
        parseAttributesCount(reader,classFile);
        parseAttributeItems(reader,classFile);
        return classFile;
    }

    private static void parseMagic(ByteReader reader,ClassFile classFile) throws IOException {
        Integer magic = 0xCAFEBABE;
        Integer temp = reader.intRead();
        boolean ok = magic.equals(temp);
        if(ok){
            classFile.setMagic(temp);
            return;
        }
        log.error("文件解析异常，文件格式不符合要求");
    }
    private static void parseMinorVersion(ByteReader reader, ClassFile classFile) throws IOException {
        short version = reader.shortRead();
        classFile.setMinor_version(version);
    }
    private static void parseMajroVersion(ByteReader reader,ClassFile classFile)throws IOException {
        short version = reader.shortRead();
        classFile.setMajro_version(version);
    }
    private static void parseConstantPoolCount(ByteReader reader,ClassFile classFile)throws IOException {
        short count = reader.shortRead();
        classFile.setConstantPoolCount(count);
    }
    /**
     * 解析常量池
     * */
    private static void parseConstantPool(ByteReader reader,ClassFile classFile)throws IOException {
        int count = classFile.getConstantPoolCount();
        ConstantPoolItem[] constantPoolItems = new ConstantPoolItem[count];
        for(int i=1;i<constantPoolItems.length;i++){
            byte tag = reader.byteRead();
            constantPoolItems[i] = ConstantItemFactory.valueOf(tag);
            constantPoolItems[i].readInfo(reader, classFile);
        }
        classFile.setConstant_pool(constantPoolItems);
    }
    private static void parseAccessFlags(ByteReader reader,ClassFile classFile)throws IOException {
        short flags = reader.shortRead();
        classFile.setAccess_flags(flags);
    }
    private static void parseThisClass(ByteReader reader,ClassFile classFile)throws IOException {
        short thisClass =  reader.shortRead();
        classFile.setThis_class(thisClass);
    }
    private static void parseSuperClass(ByteReader reader,ClassFile classFile)throws IOException {
        short superClass = reader.shortRead();
        classFile.setSuper_class(superClass);
    }
    private static void parseInterfacesCount(ByteReader reader,ClassFile classFile)throws IOException {
        short count = reader.shortRead();
        classFile.setInterfaces_count(count);
    }

    /**
     * 解析接口
     * */
    private static void parseInterfaces(ByteReader reader,ClassFile classFile)throws IOException {
//        InterfaceItem[] items = new InterfaceItem[classFile.getFields_count()];
//        classFile.setInterfaces(items);
    }
    private static void parseFieldsCount(ByteReader reader,ClassFile classFile)throws IOException {
        short count = reader.shortRead();
        classFile.setFields_count(count);
    }
    /**
     * 解析字段
     * */
    private static void parseFields(ByteReader reader,ClassFile classFile)throws IOException {
        FieldItem[] items = new FieldItem[classFile.getFields_count()];
        for(int i=0;i<items.length;i++){
            items[i] = new FieldItem();
            items[i].readInfo(reader, classFile);
        }
        classFile.setFields(items);
    }
    private static void parseMethodCount(ByteReader reader,ClassFile classFile)throws IOException {
        short count = reader.shortRead();
        classFile.setMethod_count(count);
    }
    /**
     * 解析方法
     * */
    private static void parseMethods(ByteReader reader,ClassFile classFile)throws IOException {
        MethodItem[] items = new MethodItem[classFile.getMethod_count()];
        for(int i=0;i<items.length;i++){
            items[i] = new MethodItem();
            items[i].readInfo(reader, classFile);
        }
        classFile.setMethods(items);
    }
    private static void parseAttributesCount(ByteReader reader,ClassFile classFile)throws IOException {
        short count = reader.shortRead();
        classFile.setAttributes_count(count);
    }
    /**
     * 解析参数
     * */
    private static void parseAttributeItems(ByteReader reader,ClassFile classFile)throws IOException {
        AttributeItem[] items = new AttributeItem[classFile.getAttributes_count()];
        for(int i=0;i<items.length;i++){
            items[i] = AttributeItemFacotry.valueOf(reader.shortRead(),classFile);
            items[i].readInfo(reader, classFile);
        }
        classFile.setAttributeItems(items);
    }

}
