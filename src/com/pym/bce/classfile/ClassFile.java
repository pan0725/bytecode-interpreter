package com.pym.bce.classfile;

import com.pym.bce.classfile.item.attribute.AttributeItem;
import com.pym.bce.classfile.item.constant.ConstantPoolItem;
import com.pym.bce.classfile.item.FieldItem;
import com.pym.bce.classfile.item.MethodItem;

public class ClassFile {

    private int                     magic;
    private short                   minor_version;
    private short                   majro_version;
    private short                   constantPoolCount;
    private ConstantPoolItem[]      constant_pool;
    private short                   access_flags;
    private short                   this_class;
    private short                   super_class;
    private short                   interfaces_count;
    private short[]                 interfaces;
    private short                   fields_count;
    private FieldItem[]             fields;
    private short                   method_count;
    private MethodItem[]            methods;
    private short                   attributes_count;
    private AttributeItem[]         attributeItems;


    public int getMagic() {
        return magic;
    }

    public void setMagic(int magic) {
        this.magic = magic;
    }

    public short getMinor_version() {
        return minor_version;
    }

    public void setMinor_version(short minor_version) {
        this.minor_version = minor_version;
    }

    public short getMajro_version() {
        return majro_version;
    }

    public void setMajro_version(short majro_version) {
        this.majro_version = majro_version;
    }

    public short getConstantPoolCount() {
        return constantPoolCount;
    }

    public void setConstantPoolCount(short constantPoolCount) {
        this.constantPoolCount = constantPoolCount;
    }

    public ConstantPoolItem[] getConstant_pool() {
        return constant_pool;
    }

    public void setConstant_pool(ConstantPoolItem[] constant_pool) {
        this.constant_pool = constant_pool;
    }

    public short getAccess_flags() {
        return access_flags;
    }

    public void setAccess_flags(short access_flags) {
        this.access_flags = access_flags;
    }

    public short getThis_class() {
        return this_class;
    }

    public void setThis_class(short this_class) {
        this.this_class = this_class;
    }

    public short getSuper_class() {
        return super_class;
    }

    public void setSuper_class(short super_class) {
        this.super_class = super_class;
    }

    public short getInterfaces_count() {
        return interfaces_count;
    }

    public void setInterfaces_count(short interfaces_count) {
        this.interfaces_count = interfaces_count;
    }

    public short[] getInterfaces() {
        return interfaces;
    }

    public void setInterfaces(short[] interfaces) {
        this.interfaces = interfaces;
    }

    public short getFields_count() {
        return fields_count;
    }

    public void setFields_count(short fields_count) {
        this.fields_count = fields_count;
    }

    public FieldItem[] getFields() {
        return fields;
    }

    public void setFields(FieldItem[] fields) {
        this.fields = fields;
    }

    public short getMethod_count() {
        return method_count;
    }

    public void setMethod_count(short method_count) {
        this.method_count = method_count;
    }

    public MethodItem[] getMethods() {
        return methods;
    }

    public void setMethods(MethodItem[] methods) {
        this.methods = methods;
    }

    public short getAttributes_count() {
        return attributes_count;
    }

    public void setAttributes_count(short attributes_count) {
        this.attributes_count = attributes_count;
    }

    public AttributeItem[] getAttributeItems() {
        return attributeItems;
    }

    public void setAttributeItems(AttributeItem[] attributeItems) {
        this.attributeItems = attributeItems;
    }
}
