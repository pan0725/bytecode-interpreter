package com.pym.bce.classfile.item.constant;

import com.pym.bce.classfile.ClassFile;
import com.pym.bce.util.read.ByteReader;

import java.io.IOException;

public class ConstantUtf8Info  extends ConstantPoolItem {

    private static final byte MY_TAG = 1;

    private short length;
    // 长度没有限度
    private byte[] bytes;

    @Override
    byte takeTag() {
        return MY_TAG;
    }


    @Override
    public void readInfo(ByteReader reader, ClassFile classFile) throws IOException {
        length = reader.shortRead();
        bytes = reader.read(length);
    }

    public byte[] getBytes() {
        return bytes;
    }

    @Override
    public String getValue(ConstantPoolItem[] constantPool) throws IOException {
        String s = new String(bytes);
        return s;
    }
}
