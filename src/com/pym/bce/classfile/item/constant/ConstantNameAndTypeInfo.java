package com.pym.bce.classfile.item.constant;

import com.pym.bce.classfile.ClassFile;
import com.pym.bce.util.read.ByteReader;

import java.io.IOException;

public class ConstantNameAndTypeInfo  extends ConstantPoolItem{


    private static final byte MY_TAG = 12;
    private static final byte MY_COUNT = 4;

    short nameIndex;
    short descriptorIndex;

    @Override
    byte takeTag() {
        return MY_TAG;
    }

    @Override
    public void readInfo(ByteReader reader, ClassFile classFile) throws IOException {
        nameIndex = reader.shortRead();
        descriptorIndex = reader.shortRead();
    }

    @Override
    public String[] getValue(ConstantPoolItem[] constantPool) throws IOException {
        String[] strings = new String[2];
        strings[0] = (String) constantPool[nameIndex].getValue(constantPool);
        strings[1] = (String)  constantPool[descriptorIndex].getValue(constantPool);
        return strings;
    }
}
