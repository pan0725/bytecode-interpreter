package com.pym.bce.classfile.item.constant;

import com.pym.bce.classfile.ClassFile;
import com.pym.bce.util.read.ByteReader;

import java.io.IOException;

public class ConstantDoubleInfo extends ConstantPoolItem {
    private static final byte MY_TAG = 6;
    private static final byte MY_COUNT = 8;

    int highBytes;
    int lowBytes;

    @Override
    byte takeTag() {
        return MY_TAG;
    }

    @Override
    public void readInfo(ByteReader reader, ClassFile classFile) throws IOException {
        highBytes = reader.intRead();
        lowBytes  = reader.intRead();
    }

    // TODO
    @Override
    public Double getValue(ConstantPoolItem[] constantPool) throws IOException {
        return null;
    }
}
