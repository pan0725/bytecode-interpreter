package com.pym.bce.classfile.item.constant;

import com.pym.bce.classfile.item.ClassFileItem;

import java.io.IOException;

public abstract class ConstantPoolItem implements ClassFileItem {

    private final byte tag;

    public ConstantPoolItem(){
        tag = takeTag();
    }

    /**
     * 常量池中的每个元素都有tag属性，所以继承ConstantPoolItem的所有类都需要提供tag。
     * */
    abstract byte takeTag();

    public byte getTag() {
        return tag;
    }

    public abstract Object getValue(ConstantPoolItem[] constantPool) throws IOException;

}
