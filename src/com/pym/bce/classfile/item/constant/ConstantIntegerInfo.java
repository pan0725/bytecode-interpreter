package com.pym.bce.classfile.item.constant;

import com.pym.bce.classfile.ClassFile;
import com.pym.bce.util.read.ByteReader;

import java.io.IOException;

public class ConstantIntegerInfo extends ConstantPoolItem {

    private static final byte MY_TAG = 3;
    private static final byte MY_COUNT = 4;

    int bytes;

    @Override
    byte takeTag() {
        return MY_TAG;
    }

    @Override
    public void readInfo(ByteReader reader, ClassFile classFile) throws IOException {
        bytes = reader.intRead();
    }

    @Override
    public Integer getValue(ConstantPoolItem[] constantPool) throws IOException {
        return bytes;
    }
}
