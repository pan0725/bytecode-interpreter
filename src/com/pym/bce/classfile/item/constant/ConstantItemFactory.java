package com.pym.bce.classfile.item.constant;

import com.pym.bce.util.Log;

public class ConstantItemFactory {
    
    public static ConstantPoolItem valueOf(byte tag){
        ConstantPoolItem constantPoolItem  = null;
        switch (tag){
            case ConstantTag.CLASS:
                constantPoolItem = new ConstantClassInfo(); 
                break;
            case ConstantTag.DOUBLE:
                constantPoolItem = new ConstantDoubleInfo(); 
                break;
            case ConstantTag.FIELD_REF:
                constantPoolItem = new ConstantFieldRefInfo(); 
                break;
            case ConstantTag.FLOAT:
                constantPoolItem = new ConstantFloatInfo(); 
                break;
            case ConstantTag.INTEGER:
                constantPoolItem = new ConstantIntegerInfo(); 
                break;
            case ConstantTag.INTERFACE_METHOD_REF:
                constantPoolItem = new ConstantInterfaceMethodRefInfo(); 
                break;
            case ConstantTag.INVOKE_DENAMIC:
                constantPoolItem = new ConstantInvokeDynamicInfo(); 
                break;
            case ConstantTag.LONG:
                constantPoolItem = new ConstantLongInfo(); 
                break;
            case ConstantTag.METHOD_HANDLER:
                constantPoolItem = new ConstantMethodHandlerInfo(); 
                break;
            case ConstantTag.METHOD_REF:
                constantPoolItem = new ConstantMethodRefInfo();
                break;
            case ConstantTag.METHOD_TYPE:
                constantPoolItem = new ConstantMethodTypeInfo();
                break;
            case ConstantTag.NAME_AND_TYPE:
                constantPoolItem = new ConstantNameAndTypeInfo();
                break;
            case ConstantTag.STRING:
                constantPoolItem = new ConstantStringInfo();
                break;
            case ConstantTag.UTF8:
                // UTF8的处理最为特别，因为UTF8中还需要得知字符串的长度，然后才能读取,将这种特殊的逻辑不被封装在类的内部，风险很大
                // 但是如果封装则需要大量修改接口！(已解决)
                constantPoolItem = new ConstantUtf8Info();
                break;
            default:
                Log.log().error("常量池中出现无法解析的类型 : " + tag);
        }
        return constantPoolItem;
    }
}
