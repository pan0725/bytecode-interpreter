package com.pym.bce.classfile.item.constant;

import com.pym.bce.classfile.ClassFile;
import com.pym.bce.classfile.ClassLoader;
import com.pym.bce.mate.KlassMate;
import com.pym.bce.util.read.ByteReader;

import java.io.IOException;

public class ConstantClassInfo extends ConstantPoolItem {

    private static final byte MY_TAG = 7;
    private static final byte MY_COUNT = 2;

    /**原始class文件属性*/
    short nameIndex;


    @Override
    byte takeTag() {
        return MY_TAG;
    }

    @Override
    public void readInfo(ByteReader reader, ClassFile classFile) throws IOException {
        nameIndex = reader.shortRead();
    }

    @Override
    public KlassMate getValue(ConstantPoolItem[] constantPool) throws IOException {
        KlassMate klassMate = null;
        ConstantPoolItem temp = constantPool[nameIndex];
        Object o = temp.getValue(constantPool);
        if(o instanceof String){
            String res = (String) o;
            klassMate = ClassLoader.classLoader().loadClass(res.replaceAll("/","."));
        }else if(o instanceof byte[]){
            klassMate = ClassLoader.classLoader().loadClass(new String((byte[]) o));
        }
        return klassMate;
    }

    public short getNameIndex(){
        return this.nameIndex;
    }
}
