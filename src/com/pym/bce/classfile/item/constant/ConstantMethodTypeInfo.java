package com.pym.bce.classfile.item.constant;

import com.pym.bce.classfile.ClassFile;
import com.pym.bce.util.read.ByteReader;

import java.io.IOException;

public class ConstantMethodTypeInfo  extends ConstantPoolItem {

    private static final byte MY_TAG = 16;
    private static final byte MY_COUNT = 2;

    short descriptorIndex;

    @Override
    byte takeTag() {
        return MY_TAG;
    }

    @Override
    public void readInfo(ByteReader reader, ClassFile classFile) throws IOException {
        descriptorIndex = reader.shortRead();
    }

    @Override
    public Object getValue(ConstantPoolItem[] constantPool) throws IOException {
        return null;
    }
}
