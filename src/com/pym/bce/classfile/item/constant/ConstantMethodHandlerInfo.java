package com.pym.bce.classfile.item.constant;

import com.pym.bce.classfile.ClassFile;
import com.pym.bce.util.read.ByteReader;

import java.io.IOException;

public class ConstantMethodHandlerInfo  extends ConstantPoolItem {

    private static final byte MY_TAG = 15;
    private static final byte MY_COUNT = 3;

    byte referenceKind;
    short referenceIndex;

    @Override
    byte takeTag() {
        return MY_TAG;
    }

    @Override
    public void readInfo(ByteReader reader, ClassFile classFile) throws IOException {
        referenceKind = reader.byteRead();
        referenceIndex = reader.shortRead();
    }

    @Override
    public Integer getValue(ConstantPoolItem[] constantPool) throws IOException {
        return null;
    }
}
