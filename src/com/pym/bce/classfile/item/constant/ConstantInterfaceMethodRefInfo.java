package com.pym.bce.classfile.item.constant;

import com.pym.bce.classfile.ClassFile;
import com.pym.bce.util.read.ByteReader;

import java.io.IOException;

public class ConstantInterfaceMethodRefInfo extends ConstantPoolItem {

    private static final byte MY_TAG = 11;
    private static final byte MY_COUNT = 4;

    short classIndex;
    short nameAndTypeIndex;

    @Override
    byte takeTag() {
        return MY_TAG;
    }

    @Override
    public void readInfo(ByteReader reader, ClassFile classFile) throws IOException {
        classIndex = reader.shortRead();
        nameAndTypeIndex = reader.shortRead();
    }

    @Override
    public Object getValue(ConstantPoolItem[] constantPool) throws IOException {
        return null;
    }
}
