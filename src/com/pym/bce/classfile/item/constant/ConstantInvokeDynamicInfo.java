package com.pym.bce.classfile.item.constant;

import com.pym.bce.classfile.ClassFile;
import com.pym.bce.util.read.ByteReader;

import java.io.IOException;

public class ConstantInvokeDynamicInfo extends ConstantPoolItem {

    private static final byte MY_TAG = 18;
    private static final byte MY_COUNT = 4;

    short bootstrapMethodAttrIndex;
    short nameAndTypeIndex;



    @Override
    byte takeTag() {
        return MY_TAG;
    }

    @Override
    public void readInfo(ByteReader reader, ClassFile classFile) throws IOException {
        bootstrapMethodAttrIndex = reader.shortRead();
        nameAndTypeIndex = reader.shortRead();
    }

    @Override
    public Object getValue(ConstantPoolItem[] constantPool) throws IOException {
        return null;
    }
}
