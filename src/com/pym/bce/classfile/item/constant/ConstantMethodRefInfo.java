package com.pym.bce.classfile.item.constant;

import com.pym.bce.classfile.ClassFile;
import com.pym.bce.mate.KlassMate;
import com.pym.bce.mate.MethodMate;
import com.pym.bce.util.read.ByteReader;

import java.io.IOException;

public class ConstantMethodRefInfo  extends ConstantPoolItem {

    private static final byte MY_TAG = 10;
    private static final byte MY_COUNT = 4;

    short classIndex;
    short nameAndTypeIndex;

    @Override
    byte takeTag() {
        return MY_TAG;
    }

    @Override
    public void readInfo(ByteReader reader, ClassFile classFile) throws IOException {
        classIndex = reader.shortRead();
        nameAndTypeIndex = reader.shortRead();
    }

    @Override
    public MethodMate getValue(ConstantPoolItem[] constantPool) throws IOException {
        // TODO
        KlassMate klassMates = (KlassMate) constantPool[classIndex].getValue(constantPool);
        String[] strings = (String[]) constantPool[nameAndTypeIndex].getValue(constantPool);
        MethodMate[] methodMates = klassMates.getMethods();
        if(methodMates!=null && strings!=null){
            for(MethodMate mate :methodMates){
                boolean nameOk = mate.getName().equals(strings[0]);
                boolean descOk = mate.getDescriptor().equals(strings[1]);
                if(nameOk && descOk){
                    return mate;
                }
            }
        }
        return null;
    }
}
