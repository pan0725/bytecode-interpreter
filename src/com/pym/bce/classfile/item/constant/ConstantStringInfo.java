package com.pym.bce.classfile.item.constant;

import com.pym.bce.classfile.ClassFile;
import com.pym.bce.util.read.ByteReader;

import java.io.IOException;

public class ConstantStringInfo  extends ConstantPoolItem {

    private static final byte MY_TAG = 8;
    private static final byte MY_COUNT = 2;

    short stringIndex;


    @Override
    byte takeTag() {
        return MY_TAG;
    }

    @Override
    public void readInfo(ByteReader reader, ClassFile classFile) throws IOException {
        stringIndex = reader.shortRead();
    }

    @Override
    public String getValue(ConstantPoolItem[] constantPool) throws IOException {
        Object o = constantPool[stringIndex].getValue(constantPool);
        return (String) o;
    }
}
