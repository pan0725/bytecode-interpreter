package com.pym.bce.classfile.item.constant;

import com.pym.bce.classfile.ClassFile;
import com.pym.bce.util.read.ByteReader;

import java.io.IOException;

public class ConstantLongInfo extends ConstantPoolItem {

    private static final byte MY_TAG = 5;
    private static final byte MY_COUNT = 8;

    int higtBytes;
    int lowBytes;

    @Override
    byte takeTag() {
        return MY_TAG;
    }

    @Override
    public void readInfo(ByteReader reader, ClassFile classFile) throws IOException {
        higtBytes = reader.intRead();
        lowBytes = reader.intRead();
    }

    @Override
    public Long getValue(ConstantPoolItem[] constantPool) throws IOException {
        Long l = new Long(""+higtBytes+lowBytes);
        return l;
    }
}
