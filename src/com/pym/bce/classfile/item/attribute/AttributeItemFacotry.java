package com.pym.bce.classfile.item.attribute;

import com.pym.bce.classfile.ClassFile;
import com.pym.bce.classfile.item.constant.ConstantPoolItem;
import com.pym.bce.classfile.item.constant.ConstantTag;
import com.pym.bce.classfile.item.constant.ConstantUtf8Info;

public class AttributeItemFacotry {

    private static String code = "Code";
    private static String localVariableTable = "LocalVariableTable";

    public static AttributeItem valueOf(short attrIndex, ClassFile classFile){
        AttributeItem result = null;
        /** TODO 虚拟机规范中常量池下标从1开始，这就与数组相违背，是否应该封装起来或者其他更优雅的解决方案 */
        ConstantPoolItem item = classFile.getConstant_pool()[attrIndex];
         if(ConstantTag.UTF8==(item.getTag())){
            ConstantUtf8Info attrName =  (ConstantUtf8Info)item;
            // 根据attrIndex从常量池中获取属性的名称
            String name = new String(attrName.getBytes());
            if(code.equals(name)){
                result = new CodeAttributeItem();
            }else if(localVariableTable.equals(name)){
                result = new LocalVariableTableItem();
            }else{
                result = new NoneAttributeItem();
            }
             result.setAttributeNameIndex(attrIndex);
         }
         return result;
    }
}
