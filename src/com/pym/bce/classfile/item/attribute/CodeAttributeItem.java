package com.pym.bce.classfile.item.attribute;

import com.pym.bce.classfile.ClassFile;
import com.pym.bce.classfile.item.ClassFileItem;
import com.pym.bce.util.read.ByteReader;

import java.io.IOException;

public class CodeAttributeItem extends  AttributeItem {

    private static class ExceptionTable{
        short startPc;
        short endPc;
        short handlerPc;
        short cacheType;

        public void readInfo(ByteReader reader, ClassFile classFile) throws IOException {
            startPc = reader.shortRead();
            endPc = reader.shortRead();
            handlerPc = reader.shortRead();
            cacheType = reader.shortRead();
        }
    }
    private short maxStack;
    private short maxLocals;
    private int codeLength;
    private byte[] code;
    private short exceptionTableLength;
    private ExceptionTable[] exceptionTable;
    private short attributesCount;
    private AttributeItem[] attributeItems;


    @Override
    void readInfo0(ByteReader reader, ClassFile classFile) throws IOException {
        maxStack = reader.shortRead();
        maxLocals = reader.shortRead();
        codeLength = reader.intRead();
        code = reader.read(codeLength);
        exceptionTableLength = reader.shortRead();
        exceptionTable = new ExceptionTable[exceptionTableLength];
        for(int i=0;i<exceptionTable.length;i++){
            exceptionTable[i] = new ExceptionTable();
            exceptionTable[i].readInfo(reader,classFile);
        }
        attributesCount = reader.shortRead();
        attributeItems = new AttributeItem[attributesCount];
        for(int i=0;i<attributeItems.length;i++){
            attributeItems[i] = AttributeItemFacotry.valueOf(reader.shortRead(),classFile);
            attributeItems[i].readInfo(reader,classFile);
        }
    }

    public byte[] getCode(){
        return code;
    }


    public short getMaxStack() {
        return maxStack;
    }

    public void setMaxStack(short maxStack) {
        this.maxStack = maxStack;
    }

    public short getMaxLocals() {
        return maxLocals;
    }

    public void setMaxLocals(short maxLocals) {
        this.maxLocals = maxLocals;
    }

    public int getCodeLength() {
        return codeLength;
    }

    public void setCodeLength(int codeLength) {
        this.codeLength = codeLength;
    }

    public void setCode(byte[] code) {
        this.code = code;
    }

    public short getExceptionTableLength() {
        return exceptionTableLength;
    }

    public void setExceptionTableLength(short exceptionTableLength) {
        this.exceptionTableLength = exceptionTableLength;
    }

    public ExceptionTable[] getExceptionTable() {
        return exceptionTable;
    }

    public void setExceptionTable(ExceptionTable[] exceptionTable) {
        this.exceptionTable = exceptionTable;
    }

    public short getAttributesCount() {
        return attributesCount;
    }

    public void setAttributesCount(short attributesCount) {
        this.attributesCount = attributesCount;
    }

    public AttributeItem[] getAttributeItems() {
        return attributeItems;
    }

    public void setAttributeItems(AttributeItem[] attributeItems) {
        this.attributeItems = attributeItems;
    }
}
