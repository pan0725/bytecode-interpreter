package com.pym.bce.classfile.item.attribute;

import com.pym.bce.classfile.ClassFile;
import com.pym.bce.util.read.ByteReader;

import java.io.IOException;

public class LocalVariableTableItem extends AttributeItem {

    static class LocalVariableTable {
        short startPc;
        short length;
        short nameIndex;
        short descirptorIndex;
        short index;

        public void readInfo(ByteReader reader, ClassFile classFile) throws IOException {
            startPc = reader.shortRead();
            length = reader.shortRead();
            nameIndex = reader.shortRead();
            descirptorIndex = reader.shortRead();
            index = reader.shortRead();
        }
    }

    short localVariableTableLength;
    LocalVariableTable[] localVariableTable;
    @Override
    void readInfo0(ByteReader reader, ClassFile classFile) throws IOException {
        localVariableTableLength = reader.shortRead();
        localVariableTable = new LocalVariableTable[localVariableTableLength];
        for(int i=0;i<localVariableTable.length;i++){
            localVariableTable[i] = new LocalVariableTable();
            localVariableTable[i].readInfo(reader,classFile);
        }
    }
}
