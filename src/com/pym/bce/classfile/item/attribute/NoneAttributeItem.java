package com.pym.bce.classfile.item.attribute;

import com.pym.bce.classfile.ClassFile;
import com.pym.bce.util.read.ByteReader;

import java.io.IOException;

public class NoneAttributeItem  extends  AttributeItem{

    @Override
    void readInfo0(ByteReader reader, ClassFile classFile) throws IOException {
        reader.read(getAttributeLength());
    }

}
