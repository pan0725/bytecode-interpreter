package com.pym.bce.classfile.item.attribute;

import com.pym.bce.classfile.ClassFile;
import com.pym.bce.classfile.item.ClassFileItem;
import com.pym.bce.util.read.ByteReader;

import java.io.IOException;

public abstract class AttributeItem implements ClassFileItem {

    private short attributeNameIndex;
    private int attributeLength;

    @Override
    public void readInfo(ByteReader reader, ClassFile classFile) throws IOException {
        attributeLength = reader.intRead();
        readInfo0(reader,classFile);
    }
    abstract void readInfo0(ByteReader reader, ClassFile classFile) throws IOException;

    public void setAttributeNameIndex(short index){
        this.attributeNameIndex = index;
    }

    public int getAttributeLength() {
        return attributeLength;
    }

    public short getAttributeNameIndex(){
        return attributeNameIndex;
    }
}
