package com.pym.bce.classfile.item;

import com.pym.bce.classfile.ClassFile;
import com.pym.bce.classfile.item.attribute.AttributeItem;
import com.pym.bce.classfile.item.attribute.AttributeItemFacotry;
import com.pym.bce.util.read.ByteReader;

import java.io.IOException;

public class FieldItem implements ClassFileItem {

    short accessFlags;
    short nameIndex;
    short descriptorIndex;
    short attributesCount;
    AttributeItem[]  attributes;


    @Override
    public void readInfo(ByteReader reader, ClassFile classFile) throws IOException {
        accessFlags = reader.shortRead();
        nameIndex = reader.shortRead();
        descriptorIndex = reader.shortRead();
        attributesCount = reader.shortRead();
        attributes = new AttributeItem[attributesCount];
        for(int i=0;i<attributesCount;i++){
            attributes[i] = AttributeItemFacotry.valueOf(reader.shortRead(),classFile);
            attributes[i].readInfo(reader, classFile);
        }
    }

    public short getAccessFlags() {
        return accessFlags;
    }

    public void setAccessFlags(short accessFlags) {
        this.accessFlags = accessFlags;
    }

    public short getNameIndex() {
        return nameIndex;
    }

    public void setNameIndex(short nameIndex) {
        this.nameIndex = nameIndex;
    }

    public short getDescriptorIndex() {
        return descriptorIndex;
    }

    public void setDescriptorIndex(short descriptorIndex) {
        this.descriptorIndex = descriptorIndex;
    }

    public short getAttributesCount() {
        return attributesCount;
    }

    public void setAttributesCount(short attributesCount) {
        this.attributesCount = attributesCount;
    }

    public AttributeItem[] getAttributes() {
        return attributes;
    }

    public void setAttributes(AttributeItem[] attributes) {
        this.attributes = attributes;
    }
}
