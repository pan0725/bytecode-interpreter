package com.pym.bce.classfile.item;

import com.pym.bce.classfile.ClassFile;
import com.pym.bce.util.read.ByteReader;

import java.io.IOException;

public interface ClassFileItem {

    /**
     *  将细节的读取交给各个派生类。
     * */
    void readInfo(ByteReader reader, ClassFile classFile) throws IOException;
}
