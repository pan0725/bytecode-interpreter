package com.pym.bce.classfile;

import com.pym.bce.classfile.item.FieldItem;
import com.pym.bce.classfile.item.MethodItem;
import com.pym.bce.classfile.item.attribute.AttributeItem;
import com.pym.bce.classfile.item.attribute.CodeAttributeItem;
import com.pym.bce.classfile.item.constant.ConstantClassInfo;
import com.pym.bce.classfile.item.constant.ConstantPoolItem;
import com.pym.bce.mate.ConstantPoolMate;
import com.pym.bce.mate.FieldMate;
import com.pym.bce.mate.KlassMate;
import com.pym.bce.mate.MethodMate;
import sun.reflect.ConstantPool;

import java.awt.*;
import java.io.IOException;
import java.io.InputStream;

public class ParseKlass {

    // TODO
    public static KlassMate parseKlass(InputStream stream) throws IOException {
        ClassFile classFile = ParseClassFile.parse(stream);
        return doParse(classFile);
    }

    private static KlassMate doParse(ClassFile classFile) throws IOException {
        KlassMate klassMate = new KlassMate();
        ConstantPoolMate constantPoolMate = parseConstantPool(classFile);
        KlassMate superKlass = parseSuperKlass(classFile,constantPoolMate);
//        KlassMate[] interfaces = parseInterfaces();
        MethodMate[] methodMates = parseMethodMate(classFile,klassMate);
        FieldMate[] fieldMates = parseFieldMate(classFile,klassMate);
        String name = parseKlassName(classFile);

        klassMate.setName(name);
        klassMate.setFields(fieldMates);
        klassMate.setMethods(methodMates);
        klassMate.setConstantPoolMate( constantPoolMate );
        return klassMate;
    }

    private static KlassMate parseSuperKlass(ClassFile classFile,ConstantPoolMate constantPool) throws IOException {
        short superIndex = classFile.getSuper_class();
        if(superIndex==0){
            return null;
        }
        return (KlassMate) constantPool.get(superIndex);
    }


    private static ConstantPoolMate parseConstantPool(ClassFile classFile) throws IOException {
        ConstantPoolMate constantPoolMate = new ConstantPoolMate(classFile.getConstant_pool());
        return constantPoolMate;
    }
    private static String parseKlassName(ClassFile classFile) throws IOException {
        short thisIndex = classFile.getThis_class();
        ConstantPoolItem[] items = classFile.getConstant_pool();
        ConstantClassInfo item = (ConstantClassInfo)items[thisIndex];
        short nameIndex = item.getNameIndex();
        String name = (String) items[nameIndex].getValue(items);
        return name;
    }
    private static FieldMate[] parseFieldMate(ClassFile classFile,KlassMate klassMate) throws IOException {
        FieldItem[] fieldItems = classFile.getFields();
        FieldMate[] fieldMates = new FieldMate[fieldItems.length];
        for(int i=0;i<fieldItems.length;i++){
            FieldItem fieldItem = fieldItems[i];
            fieldMates[i] = new FieldMate();

            short desc = fieldItem.getDescriptorIndex();
            short name = fieldItem.getNameIndex();
            ConstantPoolItem[] constantPool = classFile.getConstant_pool();
            String descString = (String) constantPool[desc].getValue(constantPool);
            String nameString = (String) constantPool[name].getValue(constantPool);

            fieldMates[i].setDescriptor(descString);
            fieldMates[i].setName(nameString);
            // TODO 设置属性
            AttributeItem[] attributes = fieldItem.getAttributes();

            fieldMates[i].setKlass(klassMate);
        }
        return  fieldMates;
    }
    private static MethodMate[] parseMethodMate(ClassFile classFile,KlassMate klassMate) throws IOException {
        MethodItem[] methodItems = classFile.getMethods();
        MethodMate[] methodMates = new MethodMate[methodItems.length];
        for(int i=0;i<methodItems.length;i++){
            MethodItem methodItem = methodItems[i];
            methodMates[i] = new MethodMate();

            short desc = methodItem.getDescriptorIndex();
            short name = methodItem.getNameIndex();
            ConstantPoolItem[] constantPool = classFile.getConstant_pool();
            String descString = (String) constantPool[desc].getValue(constantPool);
            String nameString = (String) constantPool[name].getValue(constantPool);

            methodMates[i].setDescriptor(descString);
            methodMates[i].setName(nameString);

            AttributeItem[] attributes = methodItem.getAttributes();
            if(attributes!=null){
                for(AttributeItem item :attributes){
                    short attrNameIndex = item.getAttributeNameIndex();
                    String attrName =  (String)constantPool[attrNameIndex].getValue(constantPool);
                    if(attrName!=null && attrName.equals("Code")){
                        CodeAttributeItem codeAttributeItem = (CodeAttributeItem)item;
                        methodMates[i].setCode(codeAttributeItem.getCode());
                        methodMates[i].setMaxLocals(codeAttributeItem.getMaxLocals());
                        methodMates[i].setMaxStack(codeAttributeItem.getMaxStack());
                    }
                }
            }
            methodMates[i].setKlass(klassMate);
        }
        return  methodMates;
    }
}
