package com.pym.bce.classpath;

import com.pym.bce.classfile.classpath.ClassNode;
import com.pym.bce.classfile.classpath.ClassNodeFactory;
import org.junit.Test;

import java.io.File;
import java.io.InputStream;
import java.lang.annotation.Target;

public class ClassNodeTest {

    @Test
    public void getClassNode(){
        ClassNode node = ClassNodeFactory.getClassPath(new File("/opt/idea_project/bytecode-executor/out/production/bytecode-executor"));
        assert node!=null;
    }

    @Test
    public void getClazz(){
        ClassNode node = ClassNodeFactory.getClassPath(new File("/opt/idea_project/bytecode-executor/out/production/bytecode-executor"));
        InputStream inputStream = node.readClassFile("com.pym.bce.memory.Ref.class");
        assert node!=null;
    }

    @Test
    public void getClassPath(){
        ClassNode node = ClassNodeFactory.getClassPath(new File("/opt/bytecode-interpreter/out/production/bytecode-interpreter"));
        InputStream inputStream = node.readClassFile("com.pym.bce.memory.Ref.class");
        assert node!=null;
    }
}
