package com.pym.bce.util;

import com.pym.bce.classfile.item.constant.ConstantTag;
import com.pym.bce.util.read.InputStreamByteReader;
import com.sun.xml.internal.messaging.saaj.util.ByteInputStream;
import org.junit.Test;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ByteReaderTest {

    @Test
    public void intReadTest() throws IOException {
        byte[] bytes = new byte[]{0,0,0,4};
        InputStream stream = new ByteInputStream(bytes,4);
        InputStreamByteReader reader  = new InputStreamByteReader(stream);
        int i = reader.intRead();
        assert i == 4;
    }
    @Test
    public void shortReadTest() throws IOException {
        byte[] bytes = new byte[]{(byte)0xCA,(byte)0xFE};
        InputStream stream = new ByteInputStream(bytes,2);
        InputStreamByteReader reader  = new InputStreamByteReader(stream);
        Short i = reader.shortRead();
        System.out.printf("%x",i);
        assert  i.equals((short)0xCAFE);
    }

    @Test
    public void bitMoveTest(){
        byte l1 = (byte) 0xCA;
        byte l2 = (byte) 0xFA;
        int  l3 = (l1<<8)+l2;
        short  l4 = (short)((l1<<8)|l2);
        System.out.printf("%x\n",( l3 ));
        System.out.printf("%x",( l4 ));
    }

    @Test
    public void bitMoveTest2(){
        byte l1 = (byte) 0xCA;
        byte l2 = (byte) 0xFA;
        byte l3 = (byte) 0xBA;
        byte l4 = (byte) 0xBE;

        int i = ((l1<<24)+(l2<<16)+(l3<<8)+l4);

        System.out.printf("%x",i);

    }


}
