/**字节码对应的方法*/
//class t{
//    /**什么都不做                                                                      */
//    private void nop             (BytecodeManager bytecode){
//
//    }
//    /**将null推送至栈顶                                                                 */
//    private void aconst_null     (BytecodeManager bytecode){
//
//    }
//    /**将int类型-1推送至栈顶                                                              */
//    private void iconst_m1       (BytecodeManager bytecode){
//
//    }
//    /**将int类型0推送至栈顶                                                               */
//    private void iconst_0        (BytecodeManager bytecode){
//
//    }
//    /**将int类型1推送至栈顶                                                               */
//    private void iconst_1        (BytecodeManager bytecode){
//
//    }
//    /**将int类型2推送至栈顶                                                               */
//    private void iconst_2        (BytecodeManager bytecode){
//
//    }
//    /**将int类型3推送至栈顶                                                               */
//    private void iconst_3        (BytecodeManager bytecode){
//
//    }
//    /**将int类型4推送至栈顶                                                               */
//    private void iconst_4        (BytecodeManager bytecode){
//
//    }
//    /**将int类型5推送至栈顶                                                               */
//    private void iconst_5        (BytecodeManager bytecode){
//
//    }
//    /**将long类型0推送至栈顶                                                              */
//    private void lconst_0        (BytecodeManager bytecode){
//
//    }
//    /**将long类型1推送至栈顶                                                              */
//    private void lconst_1        (BytecodeManager bytecode){
//
//    }
//    /**将float类型。推送至栈顶                                                             */
//    private void fconst_0        (BytecodeManager bytecode){
//
//    }
//    /**将float类型1推送至栈顶                                                             */
//    private void fconst_1        (BytecodeManager bytecode){
//
//    }
//    /**将float类型2推送至栈顶                                                             */
//    private void fconst_2        (BytecodeManager bytecode){
//
//    }
//    /**将double类型0推送至栈顶                                                            */
//    private void dconst_0        (BytecodeManager bytecode){
//
//    }
//    /**将double类型1推送至栈顶                                                            */
//    private void dconst_1        (BytecodeManager bytecode){
//
//    }
//    /**将单字节的常量值（-128~127）推送至栈顶                                                    */
//    private void bipush          (BytecodeManager bytecode){
//
//    }
//    /**将一个短整类型常量值（-32 768~32 767）推送至栈顶                                            */
//    private void sipush          (BytecodeManager bytecode){
//
//    }
//    /**将int、float或String类型常量值从常量池中推送至栈顶                                           */
//    private void ldc             (BytecodeManager bytecode){
//
//    }
//    /**将int、float或String类型常量值从常量池中推送至 栈顶（宽索引）                                     */
//    private void ldc_w           (BytecodeManager bytecode){
//
//    }
//    /**将long或double类型常量值从常量池中推送至栈顶 （宽索引）                                          */
//    private void ldc2_w          (BytecodeManager bytecode){
//
//    }
//    /**将指定的int类型本地变量推送至栈顶                                                         */
//    private void iload           (BytecodeManager bytecode){
//
//    }
//    /**将指定的long类型本地变量推送至栈顶                                                        */
//    private void load            (BytecodeManager bytecode){
//
//    }
//    /**将指定的float类型本地变量推送至栈顶                                                       */
//    private void fload           (BytecodeManager bytecode){
//
//    }
//    /**将指定的double类型本地变量推送至栈顶                                                      */
//    private void dload           (BytecodeManager bytecode){
//
//    }
//    /**将指定的引用类型本地变量推送至栈顶                                                          */
//    private void aload           (BytecodeManager bytecode){
//
//    }
//    /**将第1个int类型本地变量推送至栈顶                                                         */
//    private void iload_0         (BytecodeManager bytecode){
//
//    }
//    /**将第2个int类型本地变量推送至栈顶                                                         */
//    private void iload_1         (BytecodeManager bytecode){
//
//    }
//    /**将第3个int类型本地变量推送至栈顶                                                         */
//    private void iload_2         (BytecodeManager bytecode){
//
//    }
//    /**将第4个int类型本地变量推送至栈顶                                                         */
//    private void iload_3         (BytecodeManager bytecode){
//
//    }
//    /**将第1个long类型本地变量推送至栈顶                                                        */
//    private void lload_0         (BytecodeManager bytecode){
//
//    }
//    /**将第2个long类型本地变量推送至栈顶                                                        */
//    private void lload_1         (BytecodeManager bytecode){
//
//    }
//    /**将第3个long类型本地变量推送至栈顶                                                        */
//    private void lload_2         (BytecodeManager bytecode){
//
//    }
//    /**将第4个long类型本地变量推送至栈顶                                                        */
//    private void lload_3         (BytecodeManager bytecode){
//
//    }
//    /**将第1个float类型本地变量推送至栈顶                                                       */
//    private void fload_0         (BytecodeManager bytecode){
//
//    }
//    /**将第2个float类型本地变量推送至栈顶                                                       */
//    private void fload_1         (BytecodeManager bytecode){
//
//    }
//    /**将第3个float类型本地变量推送至栈顶                                                       */
//    private void fload_2         (BytecodeManager bytecode){
//
//    }
//    /**将第4个float类型本地变量推送至栈顶                                                       */
//    private void fload_3         (BytecodeManager bytecode){
//
//    }
//    /**将第1个double类型本地变量推送至栈顶                                                      */
//    private void dload_0         (BytecodeManager bytecode){
//
//    }
//    /**将第2个double类型本地变量推送至栈顶                                                      */
//    private void dload_1         (BytecodeManager bytecode){
//
//    }
//    /**将第3个double类型本地变量推送至栈顶                                                      */
//    private void dload_2         (BytecodeManager bytecode){
//
//    }
//    /**将第4个double类型本地变量推送至栈顶                                                      */
//    private void dload_3         (BytecodeManager bytecode){
//
//    }
//    /**将第1个引用类型本地变量推送至栈顶                                                          */
//    private void aload_0         (BytecodeManager bytecode){
//
//    }
//    /**将第2个引用类型本地变量推送至栈顶                                                          */
//    private void aload_1         (BytecodeManager bytecode){
//
//    }
//    /**将第3个引用类型本地变量推送至栈顶                                                          */
//    private void aload_2         (BytecodeManager bytecode){
//
//    }
//    /**将第4个引用类型本地变量推送至栈顶                                                          */
//    private void aload_3         (BytecodeManager bytecode){
//
//    }
//    /**将int类型数组的指定元素推送至栈顶                                                         */
//    private void iaload          (BytecodeManager bytecode){
//
//    }
//    /**将10ng类型数组的指定元素推送至栈顶                                                        */
//    private void laload          (BytecodeManager bytecode){
//
//    }
//    /**将float类型数组的指定元素推送至栈顶                                                       */
//    private void faload          (BytecodeManager bytecode){
//
//    }
//    /**将double类型数组的指定元索推送至栈顶                                                      */
//    private void daload          (BytecodeManager bytecode){
//
//    }
//    /**将引用类型数组的指定元素推送至栈顶                                                          */
//    private void aaload          (BytecodeManager bytecode){
//
//    }
//    /**将boolean或byte类型数组的指定元素推送至栈顶                                                */
//    private void baload          (BytecodeManager bytecode){
//
//    }
//    /**将char类型数组的指定元素推送至栈顶                                                        */
//    private void caload          (BytecodeManager bytecode){
//
//    }
//    /**将short类型数组的指定元素推送至栈顶                                                       */
//    private void saload          (BytecodeManager bytecode){
//
//    }
//    /**将栈顶int类型数值存入指定本地变量                                                         */
//    private void istore          (BytecodeManager bytecode){
//
//    }
//    /**将栈顶long类型数值存入指定本地变量                                                        */
//    private void lstore          (BytecodeManager bytecode){
//
//    }
//    /**将栈顶float类型数值存入指定本地变量                                                       */
//    private void fstore          (BytecodeManager bytecode){
//
//    }
//    /**将栈顶double类型数值存入指定本地变量                                                      */
//    private void dstore          (BytecodeManager bytecode){
//
//    }
//    /**将栈顶引用类型数值存入指定本地变量                                                          */
//    private void astore          (BytecodeManager bytecode){
//
//    }
//    /**将栈顶int类型数值存入第1个本地变量                                                        */
//    private void istore_0        (BytecodeManager bytecode){
//
//    }
//    /**将栈顶int类型数值存入第2个本地变量                                                        */
//    private void istore_1        (BytecodeManager bytecode){
//
//    }
//    /**将栈顶int类型数值存入第3个本地变量                                                        */
//    private void istore_2        (BytecodeManager bytecode){
//
//    }
//    /**将栈顶int类型数值存入第4个本地变量                                                        */
//    private void istore_3        (BytecodeManager bytecode){
//
//    }
//    /**将栈顶long类型数值存入第1个本地变量                                                       */
//    private void lstore_0        (BytecodeManager bytecode){
//
//    }
//    /**将栈顶long类型数值存入第2个本地变量                                                       */
//    private void lstore_1        (BytecodeManager bytecode){
//
//    }
//    /**将栈顶long类型数值存入第3个本地变量                                                       */
//    private void lstore_2        (BytecodeManager bytecode){
//
//    }
//    /**将栈顶long类型数值存入第4个本地变量                                                       */
//    private void lstore_3        (BytecodeManager bytecode){
//
//    }
//    /**将栈顶float类型数值存入第1个本地变量                                                      */
//    private void fstore_0        (BytecodeManager bytecode){
//
//    }
//    /**将栈顶float类型数值存入第2个本地变量                                                      */
//    private void fstore_1        (BytecodeManager bytecode){
//
//    }
//    /**将栈顶float类型数值存入第3个本地变量                                                      */
//    private void fstore_2        (BytecodeManager bytecode){
//
//    }
//    /**将栈顶float类型数值存入第4个本地变量                                                      */
//    private void fstore_3        (BytecodeManager bytecode){
//
//    }
//    /**将栈顶double类型数值存入第1个本地变量                                                     */
//    private void dstore_0        (BytecodeManager bytecode){
//
//    }
//    /**将栈顶double类型数值存入第2个本地变量                                                     */
//    private void dstore_1        (BytecodeManager bytecode){
//
//    }
//    /**将栈顶double类型数值存入第3个本地变量                                                     */
//    private void dstore_2        (BytecodeManager bytecode){
//
//    }
//    /**将栈顶double类型数值存入第4个本地变量                                                     */
//    private void dstore_3        (BytecodeManager bytecode){
//
//    }
//    /**将栈顶引用类型数值存入第1个本地变量                                                         */
//    private void astore_0        (BytecodeManager bytecode){
//
//    }
//    /**将栈顶引用类型数值存入第2个本地变量                                                         */
//    private void astore_1        (BytecodeManager bytecode){
//
//    }
//    /**将栈顶引用类型数值存入第3个本地变量                                                         */
//    private void astore_2        (BytecodeManager bytecode){
//
//    }
//    /**将栈顶引用类型数值存入第4个本地变量                                                         */
//    private void astore_3        (BytecodeManager bytecode){
//
//    }
//    /**将栈顶int类型数值存入指定数组的指定索引位置                                                    */
//    private void iastore         (BytecodeManager bytecode){
//
//    }
//    /**将栈顶long类型数值存入指定数组的指定索引位置                                                   */
//    private void lastore         (BytecodeManager bytecode){
//
//    }
//    /**将栈顶float类型数值存入指定数组的指定索引位置                                                  */
//    private void fastore         (BytecodeManager bytecode){
//
//    }
//    /**将栈顶double类型数值存入指定数组的指定索引位置                                                 */
//    private void dastore         (BytecodeManager bytecode){
//
//    }
//    /**将栈顶引用类型数值存入指定数组的指定索引位置                                                     */
//    private void aastore         (BytecodeManager bytecode){
//
//    }
//    /**将栈顶boolean或byte类型数值存入指定数组的指定索引位置                                           */
//    private void bastore         (BytecodeManager bytecode){
//
//    }
//    /**将栈顶char类型数值存入指定数组的指定索引位置                                                   */
//    private void castore         (BytecodeManager bytecode){
//
//    }
//    /**将栈顶short类型数值存入指定数组的指定索引位置                                                  */
//    private void sastore         (BytecodeManager bytecode){
//
//    }
//    /**将栈顶数值弹出（数值不能是long或double类型的）                                               */
//    private void pop             (BytecodeManager bytecode){
//
//    }
//    /**将栈顶的一个long或double类型的数值或两个其他 类型的数值弹出                                        */
//    private void pop2            (BytecodeManager bytecode){
//
//    }
//    /**复制栈顶数值并将复制值压入栈顶                                                            */
//    private void dup             (BytecodeManager bytecode){
//
//    }
//    /**复制栈顶值并将其插入栈顶那两个值的下面                                                        */
//    private void dup_xl          (BytecodeManager bytecode){
//
//    }
//    /**复制栈顶值并将其插入栈顶那两个或三个值的下面                                                     */
//    private void dup_x2          (BytecodeManager bytecode){
//
//    }
//    /**复制栈顶的一个long或double类型的值，或两个其 他类型的值，并将其压入栈顶                                  */
//    private void dup2            (BytecodeManager bytecode){
//
//    }
//    /**复制栈顶的一个或两个值，并将其插入栈顶那两个或三 个值的下面                                             */
//    private void dup2_x1         (BytecodeManager bytecode){
//
//    }
//    /**复制栈顶的一个或两个值，并将其插入栈顶那两个、三 个或四个值的下面                                          */
//    private void dup2_x2         (BytecodeManager bytecode){
//
//    }
//    /**将栈顶的两个数值互换（数值不能是long或double类 型的）                                           */
//    private void swap            (BytecodeManager bytecode){
//
//    }
//    /**将栈顶两int类型数值相加并将结果压入栈顶                                                      */
//    private void iadd            (BytecodeManager bytecode){
//
//    }
//    /**将栈顶两long类型数值相加并将结果压入栈顶                                                     */
//    private void ladd            (BytecodeManager bytecode){
//
//    }
//    /**将栈顶两float类型数值相加并将结果压入栈顶                                                    */
//    private void fadd            (BytecodeManager bytecode){
//
//    }
//    /**将栈顶两double类型数值相减并将结果压入栈顶                                                   */
//    private void dadd            (BytecodeManager bytecode){
//
//    }
//    /**将栈顶两int类型数值相乘并将结果压入栈顶                                                      */
//    private void isub            (BytecodeManager bytecode){
//
//    }
//    /**将栈顶两long类型数值相乘并将结果压入栈顶                                                     */
//    private void lsub            (BytecodeManager bytecode){
//
//    }
//    /**将栈顶两float类型数值相乘并将结果压入栈顶                                                    */
//    private void fsub            (BytecodeManager bytecode){
//
//    }
//    /**将栈顶两double类型数值相乘并将结果压入栈顶                                                   */
//    private void dsub            (BytecodeManager bytecode){
//
//    }
//    /**将栈顶两int类型数值相除并将结果压入栈顶                                                      */
//    private void imul            (BytecodeManager bytecode){
//
//    }
//    /**将栈顶两long类型数值相除并将结果压入栈顶                                                     */
//    private void lmul            (BytecodeManager bytecode){
//
//    }
//    /**将栈顶两float类型数值相除并将结果压入栈顶                                                    */
//    private void fmul            (BytecodeManager bytecode){
//
//    }
//    /**                                                                           */
//    private void dmul            (BytecodeManager bytecode){
//
//    }
//    /**                                                                           */
//    private void idiv            (BytecodeManager bytecode){
//
//    }
//    /**                                                                           */
//    private void ldiv            (BytecodeManager bytecode){
//
//    }
//    /**                                                                           */
//    private void fdiv            (BytecodeManager bytecode){
//
//    }
//    /**将栈顶两double类型数值相除并将结果压入栈顶  #####                                            */
//    private void ddiv            (BytecodeManager bytecode){
//
//    }
//    /**将栈顶两int类型数值作取模运算并将结果压入栈顶                                                   */
//    private void irem            (BytecodeManager bytecode){
//
//    }
//    /**将栈顶两long类型数值作取模运算并将结果压入栈顶                                                  */
//    private void lrem            (BytecodeManager bytecode){
//
//    }
//    /**将栈顶两float类型数值作取模运算并将结果压入栈顶                                                 */
//    private void frem            (BytecodeManager bytecode){
//
//    }
//    /**将栈顶两double类型数值作取模运算并将结果压入栈顶                                                */
//    private void drem            (BytecodeManager bytecode){
//
//    }
//    /**将栈顶int类型数值取负并将结果压入栈顶                                                       */
//    private void ineg            (BytecodeManager bytecode){
//
//    }
//    /**将栈顶long类型数值取负并将结果压入栈顶                                                      */
//    private void lneg            (BytecodeManager bytecode){
//
//    }
//    /**将栈float类型数值取负并将结果压入栈顶                                                      */
//    private void fneg            (BytecodeManager bytecode){
//
//    }
//    /**将栈顶double类型数值取负并将结果压入栈顶                                                    */
//    private void dneg            (BytecodeManager bytecode){
//
//    }
//    /**将int型数值左移指定位数并将结果压入栈顶                                                      */
//    private void ishl            (BytecodeManager bytecode){
//
//    }
//    /**将long型数值左移指定位数并将结果压入栈顶                                                     */
//    private void lshl            (BytecodeManager bytecode){
//
//    }
//    /**将int型数值右(带符号)移指定位数并将结果压入栈顶                                                 */
//    private void ishr            (BytecodeManager bytecode){
//
//    }
//    /**将long型数值右(带符号)移指定位数并将结果压入栈顶                                                */
//    private void lshr            (BytecodeManager bytecode){
//
//    }
//    /**将int型数值右(无符号)移指定位数并将结果压入栈顶                                                 */
//    private void iushr           (BytecodeManager bytecode){
//
//    }
//    /**将long型数值右(无符号)移指定位数并将结果压入栈顶                                                */
//    private void lushr           (BytecodeManager bytecode){
//
//    }
//    /**将栈顶两int型数值"按位与"并将结果压入栈顶                                                    */
//    private void iand            (BytecodeManager bytecode){
//
//    }
//    /**将栈顶两long型数值"按位与"并将结果压入栈顶                                                   */
//    private void land            (BytecodeManager bytecode){
//
//    }
//    /**将栈顶两int型数值"按位或"并将结果压入栈顶                                                    */
//    private void ior             (BytecodeManager bytecode){
//
//    }
//    /**将栈顶两long型数值"按位或"并将结果压入栈顶                                                   */
//    private void lor             (BytecodeManager bytecode){
//
//    }
//    /**将栈顶两int型数值"按位异或"并将结果压入栈顶    ;                                              */
//    private void ixor            (BytecodeManager bytecode){
//
//    }
//    /**将栈顶两long型数值"按位异或"并将结果压入栈顶                                                  */
//    private void lxor            (BytecodeManager bytecode){
//
//    }
//    /**将指定int型变量增加指定值(如i++, i--, i+=2等)                                           */
//    private void iinc            (BytecodeManager bytecode){
//
//    }
//    /**将栈顶int型数值强制转换为long型数值并将结果压入栈顶                                              */
//    private void i2l             (BytecodeManager bytecode){
//
//    }
//    /**将栈顶int型数值强制转换为float型数值并将结果压入栈顶                                             */
//    private void i2f             (BytecodeManager bytecode){
//
//    }
//    /**将栈顶int型数值强制转换为double型数值并将结果压入栈顶                                            */
//    private void i2d             (BytecodeManager bytecode){
//
//    }
//    /**将栈顶long型数值强制转换为int型数值并将结果压入栈顶                                              */
//    private void l2i             (BytecodeManager bytecode){
//
//    }
//    /**将栈顶long型数值强制转换为float型数值并将结果压入栈顶                                            */
//    private void l2f             (BytecodeManager bytecode){
//
//    }
//    /**将栈顶long型数值强制转换为double型数值并将结果压入栈顶                                           */
//    private void l2d             (BytecodeManager bytecode){
//
//    }
//    /**将栈顶float型数值强制转换为int型数值并将结果压入栈顶                                             */
//    private void f2i             (BytecodeManager bytecode){
//
//    }
//    /**将栈顶float型数值强制转换为long型数值并将结果压入栈顶                                            */
//    private void f2l             (BytecodeManager bytecode){
//
//    }
//    /**将栈顶float型数值强制转换为double型数值并将结果压入栈顶                                          */
//    private void f2d             (BytecodeManager bytecode){
//
//    }
//    /**将栈顶double型数值强制转换为int型数值并将结果压入栈顶                                            */
//    private void d2i             (BytecodeManager bytecode){
//
//    }
//    /**将栈顶double型数值强制转换为long型数值并将结果压入栈顶                                           */
//    private void d2l             (BytecodeManager bytecode){
//
//    }
//    /**将栈顶double型数值强制转换为float型数值并将结果压入栈顶                                          */
//    private void d2f             (BytecodeManager bytecode){
//
//    }
//    /**将栈顶int型数值强制转换为byte型数值并将结果压入栈顶                                              */
//    private void i2b             (BytecodeManager bytecode){
//
//    }
//    /**将栈顶int型数值强制转换为char型数值并将结果压入栈顶                                              */
//    private void i2c             (BytecodeManager bytecode){
//
//    }
//    /**将栈顶int型数值强制转换为short型数值并将结果压入栈顶                                             */
//    private void i2s             (BytecodeManager bytecode){
//
//    }
//    /**比较栈顶两long型数值大小, 并将结果(1, 0或-1)压入栈顶                                          */
//    private void lcmp            (BytecodeManager bytecode){
//
//    }
//    /**比较栈顶两float型数值大小, 并将结果(1, 0或-1)压入栈顶; 当其中一个数值为NaN时, 将-1压入栈顶}                 */
//    private void fcmpl           (BytecodeManager bytecode){
//
//    }
//    /**比较栈顶两float型数值大小, 并将结果(1, 0或-1)压入栈顶; 当其中一个数值为NaN时, 将1压入栈顶}                  */
//    private void fcmpg           (BytecodeManager bytecode){
//
//    }
//    /**比较栈顶两double型数值大小, 并将结果(1, 0或-1)压入栈顶; 当其中一个数值为NaN时, 将-1压入栈顶}                */
//    private void dcmpl           (BytecodeManager bytecode){
//
//    }
//    /**比较栈顶两double型数值大小, 并将结果(1, 0或-1)压入栈顶; 当其中一个数值为NaN时, 将1压入栈顶}                 */
//    private void dcmpg           (BytecodeManager bytecode){
//
//    }
//    /**当栈顶int型数值等于0时跳转}                                                           */
//    private void ifeq            (BytecodeManager bytecode){
//
//    }
//    /**当栈顶int型数值不等于0时跳转}                                                          */
//    private void ifne            (BytecodeManager bytecode){
//
//    }
//    /**当栈顶int型数值小于0时跳转}                                                           */
//    private void iflt            (BytecodeManager bytecode){
//
//    }
//    /**当栈顶int型数值大于等于0时跳转}                                                         */
//    private void ifge            (BytecodeManager bytecode){
//
//    }
//    /**当栈顶int型数值大于0时跳转}                                                           */
//    private void ifgt            (BytecodeManager bytecode){
//
//    }
//    /**当栈顶int型数值小于等于0时跳转}                                                         */
//    private void ifle            (BytecodeManager bytecode){
//
//    }
//    /**比较栈顶两int型数值大小, 当结果等于0时跳转}                                                  */
//    private void if_icmpeq       (BytecodeManager bytecode){
//
//    }
//    /**比较栈顶两int型数值大小, 当结果不等于0时跳转}                                                 */
//    private void if_icmpne       (BytecodeManager bytecode){
//
//    }
//    /**比较栈顶两int型数值大小, 当结果小于0时跳转}                                                  */
//    private void if_icmplt       (BytecodeManager bytecode){
//
//    }
//    /**比较栈顶两int型数值大小, 当结果大于等于0时跳转}                                                */
//    private void if_icmpge       (BytecodeManager bytecode){
//
//    }
//    /**比较栈顶两int型数值大小, 当结果大于0时跳转}                                                  */
//    private void if_icmpgt       (BytecodeManager bytecode){
//
//    }
//    /**比较栈顶两int型数值大小, 当结果小于等于0时跳转}                                                */
//    private void if_icmple       (BytecodeManager bytecode){
//
//    }
//    /**比较栈顶两引用型数值, 当结果相等时跳转}                                                      */
//    private void if_acmpeq       (BytecodeManager bytecode){
//
//    }
//    /**比较栈顶两引用型数值, 当结果不相等时跳转}                                                     */
//    private void if_acmpne       (BytecodeManager bytecode){
//
//    }
//    /**无条件跳转}                                                                     */
//    private void goto_           (BytecodeManager bytecode){
//
//    }
//    /**跳转至指定的16位offset位置, 并将jsr的下一条指令地址压入栈顶}                                      */
//    private void jsr             (BytecodeManager bytecode){
//
//    }
//    /**返回至本地变量指定的index的指令位置(一般与jsr或jsr_w联合使用)}                                    */
//    private void ret             (BytecodeManager bytecode){
//
//    }
//    /**用于switch条件跳转, case值连续(可变长度指令)}                                             */
//    private void tableswitch     (BytecodeManager bytecode){
//
//    }
//    /**用于switch条件跳转, case值不连续(可变长度指令)}                                            */
//    private void lookupswitch    (BytecodeManager bytecode){
//
//    }
//    /**从当前方法返回int}                                                                */
//    private void ireturn         (BytecodeManager bytecode){
//
//    }
//    /**从当前方法返回long}                                                               */
//    private void lreturn         (BytecodeManager bytecode){
//
//    }
//    /**从当前方法返回float}                                                              */
//    private void freturn         (BytecodeManager bytecode){
//
//    }
//    /**从当前方法返回double}                                                             */
//    private void dreturn         (BytecodeManager bytecode){
//
//    }
//    /**从当前方法返回对象引用}                                                               */
//    private void areturn         (BytecodeManager bytecode){
//
//    }
//    /**从当前方法返回void}                                                               */
//    private void return_         (BytecodeManager bytecode){
//
//    }
//    /**获取指定类的静态域, 并将其压入栈顶}                                                        */
//    private void getstatic       (BytecodeManager bytecode){
//
//    }
//    /**为指定类的静态域赋值}                                                                */
//    private void putstatic       (BytecodeManager bytecode){
//
//    }
//    /**获取指定类的实例域, 并将其压入栈顶}                                                        */
//    private void getfield        (BytecodeManager bytecode){
//
//    }
//    /**为指定类的实例域赋值}                                                                */
//    private void putfield        (BytecodeManager bytecode){
//
//    }
//    /**调用实例方法}                                                                    */
//    private void invokevirtual   (BytecodeManager bytecode){
//
//    }
//    /**调用超类构建方法, 实例初始化方法, 私有方法}                                                   */
//    private void invokespecial   (BytecodeManager bytecode){
//
//    }
//    /**调用静态方法}                                                                    */
//    private void invokestatic    (BytecodeManager bytecode){
//
//    }
//    /**调用接口方法}                                                                    */
//    private void invokeinterface (BytecodeManager bytecode){
//
//    }
//    /**调用动态方法}                                                                    */
//    private void invokedynamic   (BytecodeManager bytecode){
//
//    }
//    /**创建一个对象, 并将其引用引用值压入栈顶}                                                      */
//    private void new_            (BytecodeManager bytecode){
//
//    }
//    /**创建一个指定的原始类型(如int, float, char等)的数组, 并将其引用值压入栈顶}                            */
//    private void newarray        (BytecodeManager bytecode){
//
//    }
//    /**创建一个引用型(如类, 接口, 数组)的数组, 并将其引用值压入栈顶}                                        */
//    private void anewarray       (BytecodeManager bytecode){
//
//    }
//    /**获取数组的长度值并压入栈顶}                                                             */
//    private void arraylength     (BytecodeManager bytecode){
//
//    }
//    /**将栈顶的异常抛出}                                                                  */
//    private void athrow          (BytecodeManager bytecode){
//
//    }
//    /**检验类型转换, 检验未通过将抛出 ClassCastException}                                       */
//    private void checkcast       (BytecodeManager bytecode){
//
//    }
//    /**检验对象是否是指定类的实际, 如果是将1压入栈顶, 否则将0压入栈顶}                                        */
//    private void instanceof_     (BytecodeManager bytecode){
//
//    }
//    /**获得对象的锁, 用于同步方法或同步块}                                                        */
//    private void monitorenter    (BytecodeManager bytecode){
//
//    }
//    /**释放对象的锁, 用于同步方法或同步块}                                                        */
//    private void monitorexit     (BytecodeManager bytecode){
//
//    }
//    /**扩展本地变量的宽度}                                                                 */
//    private void wide            (BytecodeManager bytecode){
//
//    }
//    /**创建指定类型和指定维度的多维数组(执行该指令时, 操作栈中必须包含各维度的长度值), 并将其引用压入栈顶}                      */
//    private void multianewarray  (BytecodeManager bytecode){
//
//    }
//    /**为null时跳转                                                                   */
//    private void ifnull          (BytecodeManager bytecode){
//
//    }
//    /**不为null时跳转                                                                  */
//    private void ifnonnull       (BytecodeManager bytecode){
//
//    }
//    /**无条件跳转(宽索引)                                                                 */
//    private void goto_w	         (BytecodeManager bytecode){
//
//    }
//}

//        什么都不做
//        将null推送至栈顶
//        将int类型-1推送至栈顶
//        将int类型0推送至栈顶
//        将int类型1推送至栈顶
//        将int类型2推送至栈顶
//        将int类型3推送至栈顶
//        将int类型4推送至栈顶
//        将int类型5推送至栈顶
//        将long类型0推送至栈顶
//        将long类型1推送至栈顶
//        将float类型。推送至栈顶
//        将float类型1推送至栈顶
//        将float类型2推送至栈顶
//        将double类型0推送至栈顶
//        将double类型1推送至栈顶
//        将单字节的常量值（-128~127）推送至栈顶
//        将一个短整类型常量值（-32 768~32 767）推送至栈顶
//        将int、float或String类型常量值从常量池中推送至栈顶
//        将int、float或String类型常量值从常量池中推送至 栈顶（宽索引）
//        将long或double类型常量值从常量池中推送至栈顶 （宽索引）
//        将指定的int类型本地变量推送至栈顶
//        将指定的long类型本地变量推送至栈顶
//        将指定的float类型本地变量推送至栈顶
//        将指定的double类型本地变量推送至栈顶
//        将指定的引用类型本地变量推送至栈顶
//        将第1个int类型本地变量推送至栈顶
//        将第2个int类型本地变量推送至栈顶
//        将第3个int类型本地变量推送至栈顶
//        将第4个int类型本地变量推送至栈顶
//        将第1个long类型本地变量推送至栈顶
//        将第2个long类型本地变量推送至栈顶
//        将第3个long类型本地变量推送至栈顶
//        将第4个long类型本地变量推送至栈顶
//        将第1个float类型本地变量推送至栈顶
//        将第2个float类型本地变量推送至栈顶
//        将第3个float类型本地变量推送至栈顶
//        将第4个float类型本地变量推送至栈顶
//        将第1个double类型本地变量推送至栈顶
//        将第2个double类型本地变量推送至栈顶
//        将第3个double类型本地变量推送至栈顶
//        将第4个double类型本地变量推送至栈顶
//        将第1个引用类型本地变量推送至栈顶
//        将第2个引用类型本地变量推送至栈顶
//        将第3个引用类型本地变量推送至栈顶
//        将第4个引用类型本地变量推送至栈顶
//        将int类型数组的指定元素推送至栈顶
//        将10ng类型数组的指定元素推送至栈顶
//        将float类型数组的指定元素推送至栈顶
//        将double类型数组的指定元索推送至栈顶
//        将引用类型数组的指定元素推送至栈顶
//        将boolean或byte类型数组的指定元素推送至栈顶
//        将char类型数组的指定元素推送至栈顶
//        将short类型数组的指定元素推送至栈顶
//        将栈顶int类型数值存入指定本地变量
//        将栈顶long类型数值存入指定本地变量
//        将栈顶float类型数值存入指定本地变量
//        将栈顶double类型数值存入指定本地变量
//        将栈顶引用类型数值存入指定本地变量
//        将栈顶int类型数值存入第1个本地变量
//        将栈顶int类型数值存入第2个本地变量
//        将栈顶int类型数值存入第3个本地变量
//        将栈顶int类型数值存入第4个本地变量
//        将栈顶long类型数值存入第1个本地变量
//        将栈顶long类型数值存入第2个本地变量
//        将栈顶long类型数值存入第3个本地变量
//        将栈顶long类型数值存入第4个本地变量
//        将栈顶float类型数值存入第1个本地变量
//        将栈顶float类型数值存入第2个本地变量
//        将栈顶float类型数值存入第3个本地变量
//        将栈顶float类型数值存入第4个本地变量
//        将栈顶double类型数值存入第1个本地变量
//        将栈顶double类型数值存入第2个本地变量
//        将栈顶double类型数值存入第3个本地变量
//        将栈顶double类型数值存入第4个本地变量
//        将栈顶引用类型数值存入第1个本地变量
//        将栈顶引用类型数值存入第2个本地变量
//        将栈顶引用类型数值存入第3个本地变量
//        将栈顶引用类型数值存入第4个本地变量
//        将栈顶int类型数值存入指定数组的指定索引位置
//        将栈顶long类型数值存入指定数组的指定索引位置
//        将栈顶float类型数值存入指定数组的指定索引位置
//        将栈顶double类型数值存入指定数组的指定索引位置
//        将栈顶引用类型数值存入指定数组的指定索引位置
//        将栈顶boolean或byte类型数值存入指定数组的指定索引位置
//        将栈顶char类型数值存入指定数组的指定索引位置
//        将栈顶short类型数值存入指定数组的指定索引位置
//        将栈顶数值弹出（数值不能是long或double类型的）
//        将栈顶的一个long或double类型的数值或两个其他 类型的数值弹出
//        复制栈顶数值并将复制值压入栈顶
//        复制栈顶值并将其插入栈顶那两个值的下面
//        复制栈顶值并将其插入栈顶那两个或三个值的下面
//        复制栈顶的一个long或double类型的值，或两个其 他类型的值，并将其压入栈顶
//        复制栈顶的一个或两个值，并将其插入栈顶那两个或三 个值的下面
//        复制栈顶的一个或两个值，并将其插入栈顶那两个、三 个或四个值的下面
//        将栈顶的两个数值互换（数值不能是long或double类 型的）
//        将栈顶两int类型数值相加并将结果压入栈顶
//        将栈顶两long类型数值相加并将结果压入栈顶
//        将栈顶两float类型数值相加并将结果压入栈顶
//        将栈顶两double类型数值相减并将结果压入栈顶
//        将栈顶两int类型数值相乘并将结果压入栈顶
//        将栈顶两long类型数值相乘并将结果压入栈顶
//        将栈顶两float类型数值相乘并将结果压入栈顶
//        将栈顶两double类型数值相乘并将结果压入栈顶
//        将栈顶两int类型数值相除并将结果压入栈顶
//        将栈顶两long类型数值相除并将结果压入栈顶
//        将栈顶两float类型数值相除并将结果压入栈顶
//
//
//
//
//        将栈顶两double类型数值相除并将结果压入栈顶  #####
//        将栈顶两int类型数值作取模运算并将结果压入栈顶
//        将栈顶两long类型数值作取模运算并将结果压入栈顶
//        将栈顶两float类型数值作取模运算并将结果压入栈顶
//        将栈顶两double类型数值作取模运算并将结果压入栈顶
//        将栈顶int类型数值取负并将结果压入栈顶
//        将栈顶long类型数值取负并将结果压入栈顶
//        将栈float类型数值取负并将结果压入栈顶
//        将栈顶double类型数值取负并将结果压入栈顶
//        将int型数值左移指定位数并将结果压入栈顶
//        将long型数值左移指定位数并将结果压入栈顶
//        将int型数值右(带符号)移指定位数并将结果压入栈顶
//        将long型数值右(带符号)移指定位数并将结果压入栈顶
//        将int型数值右(无符号)移指定位数并将结果压入栈顶
//        将long型数值右(无符号)移指定位数并将结果压入栈顶
//        将栈顶两int型数值"按位与"并将结果压入栈顶
//        将栈顶两long型数值"按位与"并将结果压入栈顶
//        将栈顶两int型数值"按位或"并将结果压入栈顶
//        将栈顶两long型数值"按位或"并将结果压入栈顶
//        将栈顶两int型数值"按位异或"并将结果压入栈顶    ;
//        将栈顶两long型数值"按位异或"并将结果压入栈顶
//        将指定int型变量增加指定值(如i++, i--, i+=2等)
//        将栈顶int型数值强制转换为long型数值并将结果压入栈顶
//        将栈顶int型数值强制转换为float型数值并将结果压入栈顶
//        将栈顶int型数值强制转换为double型数值并将结果压入栈顶
//        将栈顶long型数值强制转换为int型数值并将结果压入栈顶
//        将栈顶long型数值强制转换为float型数值并将结果压入栈顶
//        将栈顶long型数值强制转换为double型数值并将结果压入栈顶
//        将栈顶float型数值强制转换为int型数值并将结果压入栈顶
//        将栈顶float型数值强制转换为long型数值并将结果压入栈顶
//        将栈顶float型数值强制转换为double型数值并将结果压入栈顶
//        将栈顶double型数值强制转换为int型数值并将结果压入栈顶
//        将栈顶double型数值强制转换为long型数值并将结果压入栈顶
//        将栈顶double型数值强制转换为float型数值并将结果压入栈顶
//        将栈顶int型数值强制转换为byte型数值并将结果压入栈顶
//        将栈顶int型数值强制转换为char型数值并将结果压入栈顶
//        将栈顶int型数值强制转换为short型数值并将结果压入栈顶
//        比较栈顶两long型数值大小, 并将结果(1, 0或-1)压入栈顶
//        比较栈顶两float型数值大小, 并将结果(1, 0或-1)压入栈顶; 当其中一个数值为NaN时, 将-1压入栈顶}
//        比较栈顶两float型数值大小, 并将结果(1, 0或-1)压入栈顶; 当其中一个数值为NaN时, 将1压入栈顶}
//        比较栈顶两double型数值大小, 并将结果(1, 0或-1)压入栈顶; 当其中一个数值为NaN时, 将-1压入栈顶}
//        比较栈顶两double型数值大小, 并将结果(1, 0或-1)压入栈顶; 当其中一个数值为NaN时, 将1压入栈顶}
//        当栈顶int型数值等于0时跳转}
//        当栈顶int型数值不等于0时跳转}
//        当栈顶int型数值小于0时跳转}
//        当栈顶int型数值大于等于0时跳转}
//        当栈顶int型数值大于0时跳转}
//        当栈顶int型数值小于等于0时跳转}
//        比较栈顶两int型数值大小, 当结果等于0时跳转}
//        比较栈顶两int型数值大小, 当结果不等于0时跳转}
//        比较栈顶两int型数值大小, 当结果小于0时跳转}
//        比较栈顶两int型数值大小, 当结果大于等于0时跳转}
//        比较栈顶两int型数值大小, 当结果大于0时跳转}
//        比较栈顶两int型数值大小, 当结果小于等于0时跳转}
//        比较栈顶两引用型数值, 当结果相等时跳转}
//        比较栈顶两引用型数值, 当结果不相等时跳转}
//        无条件跳转}
//        跳转至指定的16位offset位置, 并将jsr的下一条指令地址压入栈顶}
//        返回至本地变量指定的index的指令位置(一般与jsr或jsr_w联合使用)}
//        用于switch条件跳转, case值连续(可变长度指令)}
//        用于switch条件跳转, case值不连续(可变长度指令)}
//        从当前方法返回int}
//        从当前方法返回long}
//        从当前方法返回float}
//        从当前方法返回double}
//        从当前方法返回对象引用}
//        从当前方法返回void}
//        获取指定类的静态域, 并将其压入栈顶}
//        为指定类的静态域赋值}
//        获取指定类的实例域, 并将其压入栈顶}
//        为指定类的实例域赋值}
//        调用实例方法}
//        调用超类构建方法, 实例初始化方法, 私有方法}
//        调用静态方法}
//        调用接口方法}
//        调用动态方法}
//        创建一个对象, 并将其引用引用值压入栈顶}
//        创建一个指定的原始类型(如int, float, char等)的数组, 并将其引用值压入栈顶}
//        创建一个引用型(如类, 接口, 数组)的数组, 并将其引用值压入栈顶}
//        获取数组的长度值并压入栈顶}
//        将栈顶的异常抛出}
//        检验类型转换, 检验未通过将抛出 ClassCastException}
//        检验对象是否是指定类的实际, 如果是将1压入栈顶, 否则将0压入栈顶}
//        获得对象的锁, 用于同步方法或同步块}
//        释放对象的锁, 用于同步方法或同步块}
//        扩展本地变量的宽度}
//        创建指定类型和指定维度的多维数组(执行该指令时, 操作栈中必须包含各维度的长度值), 并将其引用压入栈顶}
//        为null时跳转
//        不为null时跳转
//        无条件跳转(宽索引)

//parseMagic(stream,classFile);
//parseMinorVersion(stream,classFile);
//parseMajroVersion(stream,classFile);
//parseConstantPoolCount(stream,classFile);
//parseConstantPool(stream,classFile);
//parseAccessFlags(stream,classFile);
//parseThisClass(stream,classFile);
//parseSuperClass(stream,classFile);
//parseInterfacesCount(stream,classFile);
//parseInterfaces(stream,classFile);
//parseFieldsCount(stream,classFile);
//parseFields(stream,classFile);
//parseMethodCount(stream,classFile);
//parseMethods(stream,classFile);
//parseAttributesCount(stream,classFile);
//parseAttributeItems(stream,classFile);

//public void parseMagic(InputStream stream,ClassFile classFile){}
//public void parseMinorVersion(InputStream stream,ClassFile classFile){}
//public void parseMajroVersion(InputStream stream,ClassFile classFile){}
//public void parseConstantPoolCount(InputStream stream,ClassFile classFile){}
//public void parseConstantPool(InputStream stream,ClassFile classFile){}
//public void parseAccessFlags(InputStream stream,ClassFile classFile){}
//public void parseThisClass(InputStream stream,ClassFile classFile){}
//public void parseSuperClass(InputStream stream,ClassFile classFile){}
//public void parseInterfacesCount(InputStream stream,ClassFile classFile){}
//public void parseInterfaces(InputStream stream,ClassFile classFile){}
//public void parseFieldsCount(InputStream stream,ClassFile classFile){}
//public void parseFields(InputStream stream,ClassFile classFile){}
//public void parseMethodCount(InputStream stream,ClassFile classFile){}
//public void parseMethods(InputStream stream,ClassFile classFile){}
//public void parseAttributesCount(InputStream stream,ClassFile classFile){}
//public void parseAttributeItems(InputStream stream,ClassFile classFile){}

/**
 *  模板解释器代码
 * */
//class t {
//
//    public void test() {
//        switch () {
//            case nop:
//                nop(manager);
//                break;
//            case aconst_null:
//                aconst_null(manager);
//                break;
//            case iconst_m1:
//                iconst_m1(manager);
//                break;
//            case iconst_0:
//                iconst_0(manager);
//                break;
//            case iconst_1:
//                iconst_1(manager);
//                break;
//            case iconst_2:
//                iconst_2(manager);
//                break;
//            case iconst_3:
//                iconst_3(manager);
//                break;
//            case iconst_4:
//                iconst_4(manager);
//                break;
//            case iconst_5:
//                iconst_5(manager);
//                break;
//            case lconst_0:
//                lconst_0(manager);
//                break;
//            case lconst_1:
//                lconst_1(manager);
//                break;
//            case fconst_0:
//                fconst_0(manager);
//                break;
//            case fconst_1:
//                fconst_1(manager);
//                break;
//            case fconst_2:
//                fconst_2(manager);
//                break;
//            case dconst_0:
//                dconst_0(manager);
//                break;
//            case dconst_1:
//                dconst_1(manager);
//                break;
//            case bipush:
//                bipush(manager);
//                break;
//            case sipush:
//                sipush(manager);
//                break;
//            case ldc:
//                ldc(manager);
//                break;
//            case ldc_w:
//                ldc_w(manager);
//                break;
//            case ldc2_w:
//                ldc2_w(manager);
//                break;
//            case iload:
//                iload(manager);
//                break;
//            case load:
//                load(manager);
//                break;
//            case fload:
//                fload(manager);
//                break;
//            case dload:
//                dload(manager);
//                break;
//            case aload:
//                aload(manager);
//                break;
//            case iload_0:
//                iload_0(manager);
//                break;
//            case iload_1:
//                iload_1(manager);
//                break;
//            case iload_2:
//                iload_2(manager);
//                break;
//            case iload_3:
//                iload_3(manager);
//                break;
//            case lload_0:
//                lload_0(manager);
//                break;
//            case lload_1:
//                lload_1(manager);
//                break;
//            case lload_2:
//                lload_2(manager);
//                break;
//            case lload_3:
//                lload_3(manager);
//                break;
//            case fload_0:
//                fload_0(manager);
//                break;
//            case fload_1:
//                fload_1(manager);
//                break;
//            case fload_2:
//                fload_2(manager);
//                break;
//            case fload_3:
//                fload_3(manager);
//                break;
//            case dload_0:
//                dload_0(manager);
//                break;
//            case dload_1:
//                dload_1(manager);
//                break;
//            case dload_2:
//                dload_2(manager);
//                break;
//            case dload_3:
//                dload_3(manager);
//                break;
//            case aload_0:
//                aload_0(manager);
//                break;
//            case aload_l:
//                aload_l(manager);
//                break;
//            case aload_2:
//                aload_2(manager);
//                break;
//            case aload_3:
//                aload_3(manager);
//                break;
//            case iaload:
//                iaload(manager);
//                break;
//            case laload:
//                laload(manager);
//                break;
//            case faload:
//                faload(manager);
//                break;
//            case daload:
//                daload(manager);
//                break;
//            case aaload:
//                aaload(manager);
//                break;
//            case baload:
//                baload(manager);
//                break;
//            case caload:
//                caload(manager);
//                break;
//            case saload:
//                saload(manager);
//                break;
//            case istore:
//                istore(manager);
//                break;
//            case lstore:
//                lstore(manager);
//                break;
//            case fstore:
//                fstore(manager);
//                break;
//            case dstore:
//                dstore(manager);
//                break;
//            case astore:
//                astore(manager);
//                break;
//            case istore_0:
//                istore_0(manager);
//                break;
//            case istore_l:
//                istore_l(manager);
//                break;
//            case istore_2:
//                istore_2(manager);
//                break;
//            case istore_3:
//                istore_3(manager);
//                break;
//            case lstore_0:
//                lstore_0(manager);
//                break;
//            case lstore_1:
//                lstore_1(manager);
//                break;
//            case lstore_2:
//                lstore_2(manager);
//                break;
//            case lstore_3:
//                lstore_3(manager);
//                break;
//            case fstore_0:
//                fstore_0(manager);
//                break;
//            case fstore_l:
//                fstore_l(manager);
//                break;
//            case fstore_2:
//                fstore_2(manager);
//                break;
//            case fstore_3:
//                fstore_3(manager);
//                break;
//            case dstore_0:
//                dstore_0(manager);
//                break;
//            case dstore_l:
//                dstore_l(manager);
//                break;
//            case dstore_2:
//                dstore_2(manager);
//                break;
//            case dstore_3:
//                dstore_3(manager);
//                break;
//            case astore_0:
//                astore_0(manager);
//                break;
//            case astore_1:
//                astore_1(manager);
//                break;
//            case astore_2:
//                astore_2(manager);
//                break;
//            case astore_3:
//                astore_3(manager);
//                break;
//            case iastore:
//                iastore(manager);
//                break;
//            case lastore:
//                lastore(manager);
//                break;
//            case fastore:
//                fastore(manager);
//                break;
//            case dastore:
//                dastore(manager);
//                break;
//            case aastore:
//                aastore(manager);
//                break;
//            case bastore:
//                bastore(manager);
//                break;
//            case castore:
//                castore(manager);
//                break;
//            case sastore:
//                sastore(manager);
//                break;
//            case pop:
//                pop(manager);
//                break;
//            case pop2:
//                pop2(manager);
//                break;
//            case dup:
//                dup(manager);
//                break;
//            case dup_xl:
//                dup_xl(manager);
//                break;
//            case dup_x2:
//                dup_x2(manager);
//                break;
//            case dup2:
//                dup2(manager);
//                break;
//            case dup2_x1:
//                dup2_x1(manager);
//                break;
//            case dup2_x2:
//                dup2_x2(manager);
//                break;
//            case swap:
//                swap(manager);
//                break;
//            case iadd:
//                iadd(manager);
//                break;
//            case ladd:
//                ladd(manager);
//                break;
//            case fadd:
//                fadd(manager);
//                break;
//            case dadd:
//                dadd(manager);
//                break;
//            case isub:
//                isub(manager);
//                break;
//            case lsub:
//                lsub(manager);
//                break;
//            case fsub:
//                fsub(manager);
//                break;
//            case dsub:
//                dsub(manager);
//                break;
//            case imul:
//                imul(manager);
//                break;
//            case lmul:
//                lmul(manager);
//                break;
//            case fmul:
//                fmul(manager);
//                break;
//            case dmul:
//                dmul(manager);
//                break;
//            case idiv:
//                idiv(manager);
//                break;
//            case ldiv:
//                ldiv(manager);
//                break;
//            case fdiv:
//                fdiv(manager);
//                break;
//            case ddiv:
//                ddiv(manager);
//                break;
//            case irem:
//                irem(manager);
//                break;
//            case lrem:
//                lrem(manager);
//                break;
//            case frem:
//                frem(manager);
//                break;
//            case drem:
//                drem(manager);
//                break;
//            case ineg:
//                ineg(manager);
//                break;
//            case lneg:
//                lneg(manager);
//                break;
//            case fneg:
//                fneg(manager);
//                break;
//            case dneg:
//                dneg(manager);
//                break;
//            case ishl:
//                ishl(manager);
//                break;
//            case lshl:
//                lshl(manager);
//                break;
//            case ishr:
//                ishr(manager);
//                break;
//            case lshr:
//                lshr(manager);
//                break;
//            case iushr:
//                iushr(manager);
//                break;
//            case lushr:
//                lushr(manager);
//                break;
//            case iand:
//                iand(manager);
//                break;
//            case land:
//                land(manager);
//                break;
//            case ior:
//                ior(manager);
//                break;
//            case lor:
//                lor(manager);
//                break;
//            case ixor:
//                ixor(manager);
//                break;
//            case lxor:
//                lxor(manager);
//                break;
//            case iinc:
//                iinc(manager);
//                break;
//            case i2l:
//                i2l(manager);
//                break;
//            case i2f:
//                i2f(manager);
//                break;
//            case i2d:
//                i2d(manager);
//                break;
//            case l2i:
//                l2i(manager);
//                break;
//            case l2f:
//                l2f(manager);
//                break;
//            case l2d:
//                l2d(manager);
//                break;
//            case f2i:
//                f2i(manager);
//                break;
//            case f2l:
//                f2l(manager);
//                break;
//            case f2d:
//                f2d(manager);
//                break;
//            case d2i:
//                d2i(manager);
//                break;
//            case d2l:
//                d2l(manager);
//                break;
//            case d2f:
//                d2f(manager);
//                break;
//            case i2b:
//                i2b(manager);
//                break;
//            case i2c:
//                i2c(manager);
//                break;
//            case i2s:
//                i2s(manager);
//                break;
//            case lcmp:
//                lcmp(manager);
//                break;
//            case fcmpl:
//                fcmpl(manager);
//                break;
//            case fcmpg:
//                fcmpg(manager);
//                break;
//            case dcmpl:
//                dcmpl(manager);
//                break;
//            case dcmpg:
//                dcmpg(manager);
//                break;
//            case ifeq:
//                ifeq(manager);
//                break;
//            case ifne:
//                ifne(manager);
//                break;
//            case iflt:
//                iflt(manager);
//                break;
//            case ifge:
//                ifge(manager);
//                break;
//            case ifgt:
//                ifgt(manager);
//                break;
//            case ifle:
//                ifle(manager);
//                break;
//            case if_icmpeq:
//                if_icmpeq(manager);
//                break;
//            case if_icmpne:
//                if_icmpne(manager);
//                break;
//            case if_icmplt:
//                if_icmplt(manager);
//                break;
//            case if_icmpge:
//                if_icmpge(manager);
//                break;
//            case if_icmpgt:
//                if_icmpgt(manager);
//                break;
//            case if_icmple:
//                if_icmple(manager);
//                break;
//            case if_acmpeq:
//                if_acmpeq(manager);
//                break;
//            case if_acmpne:
//                if_acmpne(manager);
//                break;
//            case goto_:
//                goto_(manager);
//                break;
//            case jsr:
//                jsr(manager);
//                break;
//            case ret:
//                ret(manager);
//                break;
//            case tableswitch:
//                tableswitch(manager);
//                break;
//            case lookupswitch:
//                lookupswitch(manager);
//                break;
//            case ireturn:
//                ireturn(manager);
//                break;
//            case lreturn:
//                lreturn(manager);
//                break;
//            case freturn:
//                freturn(manager);
//                break;
//            case dreturn:
//                dreturn(manager);
//                break;
//            case areturn:
//                areturn(manager);
//                break;
//            case return_:
//                return_(manager);
//                break;
//            case getstatic:
//                getstatic(manager);
//                break;
//            case putstatic:
//                putstatic(manager);
//                break;
//            case getfield:
//                getfield(manager);
//                break;
//            case putfield:
//                putfield(manager);
//                break;
//            case invokevirtual:
//                invokevirtual(manager);
//                break;
//            case invokespecial:
//                invokespecial(manager);
//                break;
//            case invokestatic:
//                invokestatic(manager);
//                break;
//            case invokeinterface:
//                invokeinterface(manager);
//                break;
//            case invokedynamic:
//                invokedynamic(manager);
//                break;
//            case new_:
//                new_(manager);
//                break;
//            case newarray:
//                newarray(manager);
//                break;
//            case anewarray:
//                anewarray(manager);
//                break;
//            case arraylength:
//                arraylength(manager);
//                break;
//            case athrow:
//                athrow(manager);
//                break;
//            case checkcast:
//                checkcast(manager);
//                break;
//            case instanceof_:
//                instanceof_(manager);
//                break;
//            case monitorenter:
//                monitorenter(manager);
//                break;
//            case monitorexit:
//                monitorexit(manager);
//                break;
//            case wide:
//                wide(manager);
//                break;
//            case multianewarray:
//                multianewarray(manager);
//                break;
//            case ifnull:
//                ifnull(manager);
//                break;
//            case ifnonnull:
//                ifnonnull(manager);
//                break;
//            case goto_w:
//                goto_w(manager);
//                break;
//        }
//    }
//}
//


