///**将栈顶两double类型数值相减并将结果压入栈顶                            */
//private void dsub             (BytecodeManager bytecode){}
///**将栈顶两int类型数值相乘并将结果压入栈顶                               */
//private void imul             (BytecodeManager bytecode){}
///**将栈顶两long类型数值相乘并将结果压入栈顶                              */
//private void lmul             (BytecodeManager bytecode){}
///**将栈顶两float类型数值相乘并将结果压入栈顶                             */
//private void fmul             (BytecodeManager bytecode){}
///**将栈顶两double类型数值相乘并将结果压入栈顶                            */
//private void dmul             (BytecodeManager bytecode){}
///**将栈顶两int类型数值相除并将结果压入栈顶                               */
//private void idiv             (BytecodeManager bytecode){}
///**将栈顶两long类型数值相除并将结果压入栈顶                              */
//private void ldiv             (BytecodeManager bytecode){}
///**将栈顶两float类型数值相除并将结果压入栈顶                             */
//private void fdiv             (BytecodeManager bytecode){}
///**将栈顶两double类型数值相除并将结果压入栈顶                            */
//private void ddiv             (BytecodeManager bytecode){}
///**将栈顶两int类型数值作取模运算并将结果压入栈顶                            */
//private void irem             (BytecodeManager bytecode){}
///**将栈顶两long类型数值作取模运算并将结果压入栈顶                           */
//private void lrem             (BytecodeManager bytecode){}
///**将栈顶两float类型数值作取模运算并将结果压入栈顶                          */
//private void frem             (BytecodeManager bytecode){}
///**将栈顶两double类型数值作取模运算并将结果压入栈顶                         */
//private void drem             (BytecodeManager bytecode){}
///**将栈顶int类型数值取负并将结果压入栈顶                                */
//private void ineg             (BytecodeManager bytecode){}
///**将栈顶long类型数值取负并将结果压入栈顶                               */
//private void lneg             (BytecodeManager bytecode){}
///**将栈float类型数值取负并将结果压入栈顶                               */
//private void fneg             (BytecodeManager bytecode){}
///**将栈顶double类型数值取负并将结果压入栈顶                             */
//private void dneg             (BytecodeManager bytecode){}
///**将int型数值左移指定位数并将结果压入栈顶                               */
//private void ishl             (BytecodeManager bytecode){}
///**将long型数值左移指定位数并将结果压入栈顶                              */
//private void lshl             (BytecodeManager bytecode){}
///**将int型数值右(带符号)移指定位数并将结果压入栈顶                          */
//private void ishr             (BytecodeManager bytecode){}
///**将long型数值右(带符号)移指定位数并将结果压入栈顶                         */
//private void lshr             (BytecodeManager bytecode){}
///**将int型数值右(无符号)移指定位数并将结果压入栈顶                          */
//private void iushr            (BytecodeManager bytecode){}
///**将long型数值右(无符号)移指定位数并将结果压入栈顶                         */
//private void lushr            (BytecodeManager bytecode){}
///**将栈顶两int型数值"按位与"并将结果压入栈顶                             */
//private void iand             (BytecodeManager bytecode){}
///**将栈顶两long型数值"按位与"并将结果压入栈顶                            */
//private void land             (BytecodeManager bytecode){}
///**将栈顶两int型数值"按位或"并将结果压入栈顶                             */
//private void ior	             (BytecodeManager bytecode){}
///**将栈顶两long型数值"按位或"并将结果压入栈顶                            */
//private void lor	             (BytecodeManager bytecode){}
///**将栈顶两int型数值"按位异或"并将结果压入栈顶    ;                       */
//private void ixor             (BytecodeManager bytecode){}
///**将栈顶两long型数值"按位异或"并将结果压入栈顶                           */
//private void lxor             (BytecodeManager bytecode){}
///**将指定int型变量增加指定值(如i++, i--, i+=2等)                    */
//private void iinc             (BytecodeManager bytecode){}
///**将栈顶int型数值强制转换为long型数值并将结果压入栈顶                       */
//private void i2l	             (BytecodeManager bytecode){}
///**将栈顶int型数值强制转换为float型数值并将结果压入栈顶                      */
//private void i2f	             (BytecodeManager bytecode){}
///**将栈顶int型数值强制转换为double型数值并将结果压入栈顶                     */
//private void i2d	             (BytecodeManager bytecode){}
///**将栈顶long型数值强制转换为int型数值并将结果压入栈顶                       */
//private void l2i	             (BytecodeManager bytecode){}
///**将栈顶long型数值强制转换为float型数值并将结果压入栈顶                     */
//private void l2f	             (BytecodeManager bytecode){}
///**将栈顶long型数值强制转换为double型数值并将结果压入栈顶                    */
//private void l2d	             (BytecodeManager bytecode){}
///**将栈顶float型数值强制转换为int型数值并将结果压入栈顶                      */
//private void f2i	             (BytecodeManager bytecode){}
///**将栈顶float型数值强制转换为long型数值并将结果压入栈顶}                    */
//private void f2l	             (BytecodeManager bytecode){}
///**将栈顶float型数值强制转换为double型数值并将结果压入栈顶                   */
//private void f2d	             (BytecodeManager bytecode){}
///**将栈顶double型数值强制转换为int型数值并将结果压入栈顶                     */
//private void d2i	             (BytecodeManager bytecode){}
///**将栈顶double型数值强制转换为long型数值并将结果压入栈顶                    */
//private void d2l	             (BytecodeManager bytecode){}
///**将栈顶double型数值强制转换为float型数值并将结果压入栈顶                   */
//private void d2f	             (BytecodeManager bytecode){}
///**将栈顶int型数值强制转换为byte型数值并将结果压入栈顶                       */
//private void i2b	             (BytecodeManager bytecode){}
///**将栈顶int型数值强制转换为char型数值并将结果压入栈顶                       */
//private void i2c	             (BytecodeManager bytecode){}
///**将栈顶int型数值强制转换为short型数值并将结果压入栈顶                      */
//private void i2s	             (BytecodeManager bytecode){}
///**比较栈顶两long型数值大小, 并将结果(1, 0或-1)压入栈顶                   */
//private void lcmp        	 (BytecodeManager bytecode){}
///**比较栈顶两float型数值大小, 并将结果(1, 0或-1)压入栈顶; 当其中一个数值为NaN时, 将-*/
//private void fcmpl       	 (BytecodeManager bytecode){}
///**比较栈顶两float型数值大小, 并将结果(1, 0或-1)压入栈顶; 当其中一个数值为NaN时, 将1*/
//private void fcmpg       	 (BytecodeManager bytecode){}
///**比较栈顶两double型数值大小, 并将结果(1, 0或-1)压入栈顶; 当其中一个数值为NaN时, 将*/
//private void dcmpl       	 (BytecodeManager bytecode){}
///**比较栈顶两double型数值大小, 并将结果(1, 0或-1)压入栈顶; 当其中一个数值为NaN时, 将*/
//private void dcmpg       	 (BytecodeManager bytecode){}
///**当栈顶int型数值等于0时跳转                                     */
//private void ifeq        	 (BytecodeManager bytecode){}
///**当栈顶int型数值不等于0时跳转                                    */
//private void ifne        	 (BytecodeManager bytecode){}
///**当栈顶int型数值小于0时跳转                                     */
//private void iflt        	 (BytecodeManager bytecode){}
///**当栈顶int型数值大于等于0时跳转                                   */
//private void ifge        	 (BytecodeManager bytecode){}
///**当栈顶int型数值大于0时跳转                                     */
//private void ifgt        	 (BytecodeManager bytecode){}
///**当栈顶int型数值小于等于0时跳转                                   */
//private void ifle        	 (BytecodeManager bytecode){}
///**比较栈顶两int型数值大小, 当结果等于0时跳转                            */
//private void if_icmpeq   	 (BytecodeManager bytecode){}
///**比较栈顶两int型数值大小, 当结果不等于0时跳转                           */
//private void if_icmpne   	 (BytecodeManager bytecode){}
///**比较栈顶两int型数值大小, 当结果小于0时跳转                            */
//private void if_icmplt   	 (BytecodeManager bytecode){}
///**比较栈顶两int型数值大小, 当结果大于等于0时跳转                          */
//private void if_icmpge   	 (BytecodeManager bytecode){}
///**比较栈顶两int型数值大小, 当结果大于0时跳转                            */
//private void if_icmpgt   	 (BytecodeManager bytecode){}
///**比较栈顶两int型数值大小, 当结果小于等于0时跳转                          */
//private void if_icmple   	 (BytecodeManager bytecode){}
///**比较栈顶两引用型数值, 当结果相等时跳转                                */
//private void if_acmpeq   	 (BytecodeManager bytecode){}
///**比较栈顶两引用型数值, 当结果不相等时跳转                               */
//private void if_acmpne   	 (BytecodeManager bytecode){}
///**无条件跳转                                               */
//private void goto_        	 (BytecodeManager bytecode){}
///**跳转至指定的16位offset位置, 并将jsr的下一条指令地址压入栈顶                */
//private void jsr	             (BytecodeManager bytecode){}
///**返回至本地变量指定的index的指令位置(一般与jsr或jsr_w联合使用)              */
//private void ret	             (BytecodeManager bytecode){}
///**用于switch条件跳转, case值连续(可变长度指令)                       */
//private void tableswitch	     (BytecodeManager bytecode){}
///**用于switch条件跳转, case值不连续(可变长度指令)                      */
//private void lookupswitch	 (BytecodeManager bytecode){}
///**从当前方法返回int                                          */
//private void ireturn	         (BytecodeManager bytecode){}
///**从当前方法返回long                                         */
//private void lreturn	         (BytecodeManager bytecode){}
///**从当前方法返回float                                        */
//private void freturn	         (BytecodeManager bytecode){}
///**从当前方法返回double                                       */
//private void dreturn	         (BytecodeManager bytecode){}
///**从当前方法返回对象引用                                         */
//private void areturn	         (BytecodeManager bytecode){}
///**从当前方法返回void                                         */
//private void return_	         (BytecodeManager bytecode){}
///**获取指定类的静态域, 并将其压入栈顶                                  */
//private void getstatic   	 (BytecodeManager bytecode){}
///**为指定类的静态域赋值                                          */
//private void putstatic   	 (BytecodeManager bytecode){}
///**获取指定类的实例域, 并将其压入栈顶                                  */
//private void getfield    	 (BytecodeManager bytecode){}
///**为指定类的实例域赋值                                          */
//private void putfield    	 (BytecodeManager bytecode){}
///**调用实例方法                                              */
//private void invokevirtual	 (BytecodeManager bytecode){}
///**调用超类构建方法, 实例初始化方法, 私有方法                             */
//private void invokespecial	 (BytecodeManager bytecode){}
///**调用静态方法                                              */
//private void invokestatic	 (BytecodeManager bytecode){}
///**调用接口方法                                              */
//private void invokeinterface	 (BytecodeManager bytecode){}
///**调用动态方法                                              */
//private void invokedynamic	 (BytecodeManager bytecode){}
///**创建一个对象, 并将其引用引用值压入栈顶                                */
//private void new_	         (BytecodeManager bytecode){}
///**创建一个指定的原始类型(如int, float, char等)的数组, 并将其引用值压入栈顶      */
//private void newarray    	 (BytecodeManager bytecode){}
///**创建一个引用型(如类, 接口, 数组)的数组, 并将其引用值压入栈顶                  */
//private void anewarray   	 (BytecodeManager bytecode){}
///**获取数组的长度值并压入栈顶                                       */
//private void arraylength      (BytecodeManager bytecode){}
///**将栈顶的异常抛出                                            */
//private void athrow	         (BytecodeManager bytecode){}
///**检验类型转换, 检验未通过将抛出 ClassCastException                 */
//private void checkcast   	 (BytecodeManager bytecode){}
///**检验对象是否是指定类的实际, 如果是将1压入栈顶, 否则将0压入栈顶                  */
//private void instanceof_	     (BytecodeManager bytecode){}
///**获得对象的锁, 用于同步方法或同步块                                  */
//private void monitorenter	 (BytecodeManager bytecode){}
///**释放对象的锁, 用于同步方法或同步块                                  */
//private void monitorexit	     (BytecodeManager bytecode){}
///**扩展本地变量的宽度                                           */
//private void wide        	 (BytecodeManager bytecode){}
///**创建指定类型和指定维度的多维数组(执行该指令时, 操作栈中必须包含各维度的长度值), 并将其引用压入栈顶*/
//private void multianewarray	 (BytecodeManager bytecode){}
///**为null时跳转                                            */
//private void ifnull	         (BytecodeManager bytecode){}
///**不为null时跳转                                           */
//private void ifnonnull	     (BytecodeManager bytecode){}
///**无条件跳转(宽索引)                                          */
//private void goto_w	         (BytecodeManager bytecode){}