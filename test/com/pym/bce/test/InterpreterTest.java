package com.pym.bce.test;

import com.pym.bce.interpreter.BytecodeManager;
import com.pym.bce.interpreter.DefaultBytecodeManager;
import com.pym.bce.interpreter.TemplateInterpreter;
import org.junit.Test;

import java.nio.channels.FileChannel;

public class InterpreterTest {

    /**简单的运行测试*/
    @Test
    public void interpreterStartTest() {
//        InterpreterTest()
        TemplateInterpreter interpreter = new TemplateInterpreter();
        byte[] bytes = new byte[]{0,1,0,2};
        BytecodeManager manager = new DefaultBytecodeManager(bytes);
        try {
            interpreter.loop(manager);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**位运算测试*/
    @Test
    public void bitOperation(){
        int  i = 0x000F7777;
        short low = (short)i;
        short hit = (short) (i >> 16);
        System.out.println(hit);
    }

    /**
     * 测试两数相加
     * */
//    @Test
//    public void addTest(){
//        TemplateInterpreter interpreter = new TemplateInterpreter();
//        byte[] bytes = new byte[]{0,8,8,96};// nop、iconst5、iconst5、iadd
//        BytecodeManager manager = new DefaultBytecodeManager(bytes);
//        try {
//            interpreter.loop(manager);
//        }catch (Exception e){
//            int i = (int)interpreter.operandStack.pop();
//            assert i==10;
//        }
//    }
    /**测试减法运算*/
//    @Test
//    public void subTest(){
//        TemplateInterpreter interpreter = new TemplateInterpreter();
//        // bytecode 8是iconst5，7是iconst4，测试结果5-4=1
//        byte[] bytes = new byte[]{0,8,7,100};// nop、iconst5、iconst5、iadd
//        BytecodeManager manager = new DefaultBytecodeManager(bytes);
//        try {
//            interpreter.loop(manager);
//        }catch (Exception e){
//            int i = (int)interpreter.operandStack.pop();
//            assert i==1;
//        }
//    }
}
