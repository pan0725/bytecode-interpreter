package com.pym.bce.interpreter;

import com.pym.bce.classfile.ClassLoader;
import com.pym.bce.mate.KlassMate;
import org.junit.Test;

import java.io.IOException;

public class TemplateInterpreterTest {

    /**
     *  TODO 使用起来太过繁琐，封装的太过了。
     * */
    @Test
    public void runTest(){
         ClassLoader loader =  ClassLoader.classLoader();
        loader.addClassPath("D:\\java\\java-project\\a-java-project\\IDEA-Project\\code-executor\\out\\test\\bytecode-executor");
        loader.addClassPath("D:\\java\\java-project\\a-java-project\\IDEA-Project\\code-executor\\out\\production\\bytecode-executor");
        loader.addClassPath("D:\\java\\jdk1.8\\rt");
        try {

            KlassMate klassMate =        loader.loadClass("Test");
            TemplateInterpreter interpreter = new TemplateInterpreter();
            try {
                interpreter.call(klassMate.getMethods()[1]);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}
